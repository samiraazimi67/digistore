process.env.TMPDIR = 'tmp';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var fs = require('fs');
var express = require('express'),
        expressSession = require('express-session'),
        session = require('express-sessions');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var flow = require('./flow-node.js')('tmp');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = require('./db');
var mongooseFile = require('mongoose');
var lookup = require('./models/lookup.js');
var config = require('./routes/config.js');
var shop= require('./routes/shop');
var ware= require('./routes/ware');
var routes = require('./routes/index');
var users = require('./routes/users');
var permissions = require('./routes/permissions');

var app = express();
app.use(favicon(__dirname+'/public/images/favicon.ico'));
var server = require('http').createServer(app);
var io = require('socket.io')(server);

server.listen(3000, 'localhost');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cookieParser());
app.use(expressSession({
    secret: 'a4f8071f-c873-4447-8ee2',
    cookie: {maxAge: 864000000}, //2628000000 for 1 month
    resave: true,
    saveUninitialized: true,
    store: new (require('express-sessions'))({
        storage: 'mongodb',
        instance: mongoose, // optional
        host: 'localhost', // optional
        port: 27017, // optional
        db: 'Test', // optional
        collection: 'sessions', // optional
        expire: 864000000 // optional
    })
}));
app.use('/', routes);
app.use('/users', users);


app.get('/lookup', config.lookup);
app.post('/shop/list', shop.list);
app.post('/shop/listTable', shop.listTable);
app.post('/shop/create', shop.create);
app.post('/shop/edit', shop.edit);
app.post('/shop/read', shop.read);
app.post('/shop/deleteImage', shop.deleteImage);
app.post('/shop/removeRole', shop.removeRole);
app.post('/shop/addRole', shop.addRole);
app.post('/shop/roleList', shop.roleList);
app.post('/shop/delete', shop.delete);

app.post('/wareType/list', ware.wareTypeList);
app.post('/wareType/create', ware.wareTypeCreate);
app.post('/wareType/read', ware.wareTypeRead);
app.post('/wareType/edit',ware.wareTypeEdit);
app.post('/wareType/delete',ware.wareTypeDelete);
app.post('/wareType/deleteWareTypeImage', ware.wareTypeDeleteImage);
app.post('/ware/create',ware.create);
app.post('/ware/edit',ware.edit);
app.post('/ware/read',ware.read);
app.post('/ware/similarWare',ware.similarWare);
app.post('/ware/addImage',ware.addImage);
app.post('/ware/list',ware.list);
app.post('/ware/delete',ware.delete);
app.post('/ware/existsChange',ware.existsChange);
app.post('/ware/deleteImage',ware.deleteImage);

app.get('/menu', config.menu);
app.post('/login', shop.login);
app.post('/logout', shop.logout);
app.get('/shop/session', shop.session);
app.post('/auth', shop.auth);

app.post('/permission/create', permissions.permissionCreate);
app.post('/permission/update', permissions.permissionUpdate);
app.post('/permission/delete', permissions.permissionDelete);
app.post('/permission/list', permissions.permissionList);

app.post('/role/create', permissions.roleCreate);
app.post('/role/update', permissions.roleUpdate);
app.post('/role/delete', permissions.roleDelete);
app.get('/role/list', permissions.roleList);
app.post('/role/permissions', permissions.rolePermissions);
app.post('/role/removePermission', permissions.roleRemovePermission);
app.post('/role/addPermission', permissions.roleAddPermission);


app.post('/upload', multipartMiddleware, function (req, res) {
    console.log(req);
        flow.post(req, function (status, filename, original_filename, identifier) {
                console.log('POST', status, filename, identifier);
                res.send(200, {
                        // NOTE: Uncomment this funciton to enable cross-domain request.
                        //'Access-Control-Allow-Origin': '*'
                });
        });
});
// Handle status checks on chunks through Flow.js
app.get('/upload', function (req, res) {
    console.log(req);
        flow.get(req, function (status, filename, original_filename, identifier) {
            console.log('POST', status, filename, identifier);
                res.send(status == 'found' ? 200 : 404);
        });
});
/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

