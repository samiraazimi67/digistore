exports.type = {
    categories: [
        {id: 0, name: 'زنانه',icon:'fa fa-fw fa-female'},
        {id: 1, name: 'مردانه',icon:'fa fa-fw fa-user'},
        {id: 2, name: 'بچگانه',icon:'fa fa-fw fa-child'},
        
    ],
    boolean: [
        {id: 0, name: 'خیر'},
        {id: 1, name: 'بله'}
    ]
   

};
controlId = function (array)
{
    for (var i = 0; i < array.length; i++)
        if (array[i].id != i)
            return false;

    return true;

};



exports.controlIds = function ()
{
    res = controlId(exports.type.categories);
    res = res && controlId(exports.type.boolean);
    
    
    return res;
};
exports.menuItem= function ()
{
    menu=exports.type.categories;
    return menu;
};