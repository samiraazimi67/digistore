var mongoose = require('mongoose');
var db = require('./../db');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var rbac = require('mongoose-rbac');
var deepPopulate = require('mongoose-deep-populate');

var shopSchema = new Schema({
    name:String,
    clientName:String,
    address:String,
    tel:String,
    mobile:String,
    image: {type: ObjectId},
    imageFile:{},
    Email:String,
    off:Number,
    googleMapAddress:String,
    username:String,
    password:String,
    roles: [ObjectId]
    
    
});
shopSchema.plugin(rbac.plugin);
db.main.model('shop', shopSchema);
shopSchema.pre('remove', function(next) {
    // Remove all the assignment docs that reference the removed person.
    this.model('ware').remove({ supplier: this._id }, next);
});