var mongoose = require('mongoose');
var db = require('./../db');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var deepPopulate = require('mongoose-deep-populate');

var wareTypeSchema = new Schema({
    name:String,
    categories:[Number],
    icon: String,
    image: {type: ObjectId},
    imageFile:{},
    wareCount:Number,
});
db.main.model('wareType', wareTypeSchema);


var wareSchema = new Schema({
    wareType: {type: ObjectId, ref: 'wareType'},
    code:String,
    exists:{type:Boolean, default:true},
    trCreate:Number,
    categories:Number,
    color:[String],
    size:[String],
    material:String,
    mark:String,
    price:{type:Number, default:0},
    off:Number,
    tag:[String],
    supplier:{type: ObjectId, ref: 'shop'},
    dist:String,
    comment:[String],
    score:Number,
    clickCount:Number,
    pic:[String],
    picFile:[{}],
    

});
db.main.model('ware', wareSchema);
