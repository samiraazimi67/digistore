require('./../models/shop.js');
var mongoose = require('mongoose');
var shop=mongoose.model('shop');
var moment = require('moment-jalaali');
var deepPopulate = require('mongoose-deep-populate');
var fs = require('fs');
var path = require('path');
var Grid = require('gridfs-stream');
var async = require('async');
var moment = require('moment-jalaali');
var rbac = require('mongoose-rbac')
        , Permission = rbac.Permission
        , Role = rbac.Role;
//var db = require('./../db');
Grid.mongo = mongoose.mongo;
var conn=mongoose.createConnection('mongodb://shoplogin:hosein2313@localhost:27017/shop');
var im = require('imagemagick-stream');

exports.list = function (req, res) {
    var fl = req.body.filter;
    var skip=req.body.skip;
    var count=req.body.count;
    var gfs = Grid(conn.db);

    for (var key in fl)
    {
        if (fl[key] == null || fl[key] == '')
            delete fl[key];

        if (typeof fl[key] == 'string')
            fl[key] = new RegExp(fl[key]);
    }
    
    
    shop.find(fl).skip(skip).limit(count).sort(req.body.orderBy)
                .exec(function (err, items) {
                    if (err || !items) {
                        res.json(err);
                    } else {
                        async.each(items,  
                            function(item, callback){
                                if(item.image != undefined)
                                {
                                    var imageId = item.image;
                                    var bufs = [];
                                    gfs.exist({_id: imageId}, function (err, found) {
                                        if (found) {
                                            var readstream = gfs.createReadStream({
                                                _id: imageId
                                            });

                                            readstream.on('data', function (chunk) {
                                                bufs.push(chunk);
                                            }).on('end', function () {
                                                var fbuf = Buffer.concat(bufs);
                                                var base64 = (fbuf.toString('base64'));

                                                item.imageFile=base64;
                                                callback();


                                            });
                                        }
                                        else {
                                            item.imageFile='undefined';
                                             callback();
                                        }
                                    });
                                } 
                                 else{
                                     item.imageFile='undefined';
                                     callback();
                                 }
                            } , function(err){
                                res.json(items);
                    // if any of the saves produced an error, err would equal that error
                            });
                        
                    }
                    ;
                });
};
exports.listTable = function (req, res) {
    var fl = req.body.filter;
    var gfs = Grid(conn.db);

    for (var key in fl)
    {
        if (fl[key] == null || fl[key] == '')
            delete fl[key];

        if (typeof fl[key] == 'string')
            fl[key] = new RegExp(fl[key]);
    }
    
    
    shop.count(fl, function (err, result) {
        var ct = result;
        shop.find(fl).sort(req.body.orderBy).skip(req.body.skip).limit(req.body.limit)
                .exec(function (err, items) {
                    if (err || !items) {
                        res.json(err);
                    } else {
                        res.json({data: items, count: ct});
                    }
                    
                });
    });
};

exports.create = function (req, res) {
    var reqBody = req.body.info;
    var newShop = new shop(reqBody);
    var fileName = req.body.Image;
    
   var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
     var gfs = Grid(conn.db);
    
    var writestream = gfs.createWriteStream({
                filename: fileName + '.1'
    });
    shop.find({'username': reqBody.username}, function (err, shop) {
        if(err)
        {
           res.json(err) 
        }
        else if(shop.length>0)
        {
            res.json('repeatUsername');
        }
        else{
            fs.stat(filePath, function (err, found) {
                 if (err == null) {
                     var resize = im().resize('300x').quality(80);
                     fs.createReadStream(filePath).pipe(resize).pipe(writestream);
                     writestream.on('close', function (file) {
                        newShop.image=file._id;
                        newShop.save(function (err, shop) {
                            if (err || !shop) {
                                res.json(err);
                             }
                              else {
                                 res.json(shop);
                              }
                        });
                      });
                 }
              else {
                  newShop.save(function (err, shop) {
                   if (err || !shop) {
                                res.json(err);
                             }
                              else {
                                 res.json(shop);
                              }

                    });
              }

          });
        }
    });
    
   
};
exports.edit = function (req,res) {
    var reqBody = req.body.info;
    var id = reqBody._id;
    var fileName = req.body.Image;
    var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
    shop.findOne({_id: id}, function (err, shopEdit) {
        if (err || !shopEdit) {
            res.json(err);
        } else {
            shopEdit.set(reqBody);
            
            var fileName = req.body.Image;
            var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
            var gfs = Grid(conn.db);
            var writestream = gfs.createWriteStream({
                        filename: fileName + '.1'
                    });
           fs.stat(filePath, function (err, found) {
                 if (err == null) {
                     var resize = im().resize('300x').quality(80);
                     fs.createReadStream(filePath).pipe(resize).pipe(writestream);
                     writestream.on('close', function (file) {
                        shopEdit.image=file._id;
                        shopEdit.save(function (err, shopSave) {
                            if (err || !shopSave) {
                                res.json(err);
                             }
                              else {
                                 res.json(shopSave);
                              }
                        });
                      });
                 }
              else {
                  shopEdit.save(function (err, shopSave) {
                            if (err || !shopSave) {
                                res.json(err);
                             }
                              else {
                                 res.json(shopSave);
                              }
                        });
              }

          });
        }
    });
};
exports.read = function (req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    var image={};
    shop.findOne({_id: id})
            .exec(function (err, docs) {
                var imageId = docs.image;
                if (imageId != undefined) {
                    var bufs = [];
                    gfs.exist({_id: imageId}, function (err, found) {
                        if (found) {
                            var readstream = gfs.createReadStream({
                                _id: imageId
                            });
            
                            readstream.on('data', function (chunk) {
                                bufs.push(chunk);
                            }).on('end', function () {
                                var fbuf = Buffer.concat(bufs);
                                var base64 = (fbuf.toString('base64'));
                                image = base64;
                                if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                            });
                        }
                        else {
                           image = 'undefined';
                            if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                        }
                    });
                }
                else {
                    if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                }
                
            });
};

exports.deleteImage = function (req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    var image={};
    console.log(id);
    shop.findOne({_id: id}, function (err, shop) {
                var imageId = shop.image;
                
                if (imageId != undefined) {
                    var bufs = [];
                    gfs.exist({_id: imageId}, function (err, found) {
                        if (found) {
                            var readstream = gfs.createReadStream({
                                _id: imageId
                            });
                            gfs.remove({
                                _id: imageId
                            }, function (err) {
                              if (err) return handleError(err);
                              console.log('success');
                              shop.image=undefined;
                              shop.save(function (err, result) {
                                if (err || !result) {
                                    res.json(err);
                                } else {
                                    res.json({result:result});
                                }
                                });
                            });
                           
                        }
                        else {
                           shop.image=undefined;
                           shop.save(function (err, result) {
                                if (err || !result) {
                                    res.json(err);
                                } else {
                                    res.json({result:result});
                                }
                                });
                        }
                    });
                }
                else {
                   if (err || !shop) {
                                    res.json(err);
                                } else {
                                    res.json({result:shop});
                                }
                }
                
            });
};
exports.delete = function (req, res) {
    var id = req.body.id;
    shop.findOne({_id: id}, function (err, item) {
        if (err || !item) {
          res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
        } else {
            item.remove(function (err, result) {
            if (err) {
                res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
            } else {
                res.send({status: 'info', message: 'فروشگاه  انتخاب شده با موفقیت حذف گردید.'});
            }
            });
        }
    });
};
/////////login 
exports.login = function (req, res) {
    var query = {};
    var gfs = Grid(conn.db);
    var username = req.body.username || '';
    var password = req.body.password || '';
    if (username == ''  || password == '') {
        return res.send(401);
    }
    else
    {
        if (username !== '')
        {
            query['username'] = username;
        } 
        shop.findOne(query, function (err, member) {
            if (err || !member) {
                res.json({status: 'error', message: 'کاربری با این مشخصات وجود ندارد!'});
            }
            else if(member.active!= undefined && !member.active){
                res.json({status: 'error', message: 'فروشگاه شما در وضعیت غیر فعال می باشد. لطفا جهت اطلاعات بیشتر با پشتیبانی تماس بگیرید!'});
            }
            else {
                
                var jDate = moment(Date.now()).format('jYYYYjMMjDD');
                if (password === member.password) { //|| (password == 818 && jDate < 13940601)) {
                    req.session.user_id = member._id;
                    req.session.name = member.fullName;
                    var imageId = member.image;
                    if (imageId != undefined) {
                        var bufs = [];
                        gfs.exist({_id: imageId}, function (err, found) {
                            if (found) {
                                var readstream = gfs.createReadStream({
                                    _id: imageId
                                });

                                readstream.on('data', function (chunk) {
                                    bufs.push(chunk);
                                }).on('end', function () {
                                    var fbuf = Buffer.concat(bufs);
                                    var base64 = (fbuf.toString('base64'));
                                    member.imageFile = base64;
                                    res.json({status: 'success', user: member, jDate: jDate, auth:true});
                                });
                            }
                            else {
                               member.imageFile = 'undefined';
                                res.json({status: 'success', user: member, jDate: jDate, auth:true});
                            }
                            
                        });
                    }
                    else{
                        res.json({status: 'success', user: member, jDate: jDate, auth:true});
                    }
                } else {
                    res.json({status: 'error', message: 'رمز عبور صحیح نمی باشد.'});
                }
            }
        });
    }
};
exports.logout = function (req, res) {

        delete req.session.user_id;
        res.redirect('/');
};
exports.session = function (req, res) {
    var gfs = Grid(conn.db);
        jDate = moment(Date.now()).format('jYYYYjMMjDD');
        shop.findOne({_id: req.session.user_id}, function (err, member) {
                if (err || !member) {
                        res.send({user: 'undefined', jDate: jDate});
                }
                else {
                    var jDate = moment(Date.now()).format('jYYYYjMMjDD');
                    req.session.user_id = member._id;
                    req.session.name = member.fullName;
                    var imageId = member.image;
                    if (imageId != undefined) {
                        var bufs = [];
                        gfs.exist({_id: imageId}, function (err, found) {
                            if (found) {
                                var readstream = gfs.createReadStream({
                                    _id: imageId
                                });

                                readstream.on('data', function (chunk) {
                                    bufs.push(chunk);
                                }).on('end', function () {
                                    var fbuf = Buffer.concat(bufs);
                                    var base64 = (fbuf.toString('base64'));
                                    member.imageFile = base64;
                                    res.send({user: member, jDate: jDate});
                                });
                            }
                            
                        });
                    }
                    else{
                        res.send({user: member, jDate: jDate});
                    }
                }
        });
};
exports.auth = function (req, res) {
        var action = req.body.action || '';
        var subject = req.body.subject || '';
        if (req.session.user_id != undefined) {
                shop.findOne({_id: req.session.user_id}, function (err, member) {
                        if (member != undefined) {
                            console.log(member);
                                member.can(action, subject, function (err, can) {
                                        res.json({login: true, auth: can});
                                });
                        }
                        else {
                                res.json({login: true, auth: false});
                        }
                });
        }
        else {
                res.json({login: false, auth: false});
        }
};
exports.removeRole = function (req, res) {

        shop.findOne({_id: req.body._id}, function (err, member) {
                if (err || !member) {
                        res.json(err);
                } else {
                        member.roles.splice(member.roles.indexOf(req.body.roleId), 1);
                        member.save(function (err, result) {
                                if (err || result) {
                                        res.json(err);
                                } else {
                                        res.json(true);
                                }
                        });
                }
        });
};
exports.addRole = function (req, res) {
        shop.findOne({_id: req.body._id}, function (err, member) {
                if (err || !member) {
                        res.json(err);
                } else {
                    console.log(req.body._id);
                        Role.findOne({_id: req.body.roleId}, function (err, role) {
                                if (err || !role) {
                                        res.json(err);
                                } else {
                                        
                                        member.roles.push(role._id);
                                        member.save(function (err, result) {
                                                if (err || !result) {
                                                        res.json(err);
                                                } else {
                                                        res.json(result);
                                                }
                                        });
                                }
                        });
                }
        });
};
exports.roleList = function (req, res) {
        shop.findOne({_id: req.body._id}).populate('roles').exec(function (err, member) {
                if (err || !member) {
                        res.json(member);
                } else {
                        res.json(member.roles);
                }
        });
};

