var mongoose = require('mongoose');
var db = require('./../db');
var rbac = require('mongoose-rbac')
  , Permission = rbac.Permission
  , Role = rbac.Role
 
    ;
//ar members=require('./../routes/members.js');

exports.roleCreate = function (req, res) {
	var role = new Role({ name: req.body.name });

	role.save(function (err, result) {
		if(err || result) {
			res.json(err);
		} else {
			res.json(true);
		}
	});

};

exports.roleUpdate = function (req, res) {
	var reqBody = req.body;
	var id = reqBody._id;
	Role.findOne({_id: id}, function (err, doc) {
		if (err || !doc) {
			res.json(err);
		} else {
			//doc.set(reqBody);
			doc.name = reqBody.name ;
			doc.save(function (err, result) {
				if (err || !result) {
					res.json(err);
				} else {
					res.json(result);
				}
			});
		}
	});
	
};
exports.permissionUpdate = function (req, res) {
	var reqBody = req.body;
	var id = reqBody._id;
			
	Permission.findOne({_id: id}, function (err, doc) {
		if (err || !doc) {
			res.json(err);
		} else {
			doc.set(reqBody);
		
			doc.save(function (err, result) {
				if (err || !result) {
					res.json(err);
				} else {
					res.json(result);
				}
			});
		}
	});
	
};
exports.roleDelete = function (req, res) {
	Role.findOne({_id: req.body.id}, function (err, role) {
		if(err || !role) {
			res.json(err);
		} else {
			role.remove(function (err, result) {
				if(err || result) {
					res.json(err);
				} else {
					res.json(true);
				}
			});
		} 
	});
	

};
exports.roleList = function(req, res) {
	Role.find().populate('permissions').exec(function (err, roles) {
		if (err || !roles) {
			res.json(err);
		} else {
			res.json(roles);
		}
	});
};
exports.rolePermissions = function (req, res) {
	Role.findOne({name:req.body.roleName}).populate('permissions').exec(function (err, role) {
		if (err || !role) {
			res.json(err);
		} else {
			res.json(role.permissions);
		}
	});
};

exports.roleRemovePermission = function (req, res) {

		Role.findOne({name: req.body.roleName}, function (err, role) {
			if(err || !role) {
				res.json(err);
			} else {
				role.permissions.splice(role.permissions.indexOf(req.body.permissionId),1);
				role.save(function (err, result) {
					if(err || result) {
						res.json(err);
					} else {
						res.json(true);
					}
				});
			} 
		});
	
};
exports.roleAddPermission = function (req, res) {

		Role.findOne({_id: req.body.roleIdToAdd}, function (err, role) {
			if(err || !role) {
				res.json(err);
			} else {
				role.permissions.push(req.body.permissionId);
				role.save(function (err, result) {
					if(err || result) {
						res.json(err);
					} else {
						res.json(true);
					}
				});
			} 
		});
	
};
exports.permissionCreate = function (req, res) {
	var permission = new Permission({ subject: req.body.subject, action: req.body.action });

	permission.save(function (err, result) {
		if(err || result) {
			res.json(err);
		} else {
			res.json(true);
		}
	});

};

exports.permissionDelete = function (req, res) {
	Permission.findOne({_id: req.body.id}, function (err, permission) {
		if(err || !permission) {
			res.json(err);
		} else {
			permission.remove(function (err, result) {
				if(err || result) {
					res.json(err);
				} else {
					res.json(true);
				}
			});
		} 
	});
	

};
exports.permissionList = function (req, res) {
	Permission.find().exec(function (err, permissions) {
		if (err || !permissions) {
			res.json(err);
		} else {
			res.json(permissions);
		};
		});
};