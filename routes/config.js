require('./../models/ware.js');
var lookup = require('./../models/lookup.js');
var mongoose = require('mongoose');
var wareType=mongoose.model('wareType');
var ObjectID = mongoose.Schema.ObjectId;
var async = require('async');
var Grid = require('gridfs-stream');
var format = require('util').format;
var conn=mongoose.createConnection('mongodb://shoplogin:hosein2313@localhost:27017/shop');
exports.lookup = function (req, res) {
	
    res.json(lookup.type);
};

exports.menu = function (req, res) {
    var menu =lookup.menuItem();
    var gfs = Grid(conn.db);
    async.each(menu,  
                     function(item, callback){
                        item.submenu=[];
                        wareType.find({categories:item.id}).exec(function(err,wareTypes){
                                     if (err || !wareTypes) {
                                        res.json(err);
                                     } else {
                                         async.each(wareTypes,  
                                                    function(wareTypeItem, callback){
                                                            var imageId = wareTypeItem.image;
                                                            var bufs = [];
                                                            gfs.exist({_id: imageId}, function (err, found) {
                                                                if (found) {
                                                                    var readstream = gfs.createReadStream({
                                                                        _id: imageId
                                                                    });

                                                                    readstream.on('data', function (chunk) {
                                                                        bufs.push(chunk);
                                                                    }).on('end', function () {
                                                                        var fbuf = Buffer.concat(bufs);
                                                                        var base64 = (fbuf.toString('base64'));

                                                                        wareTypeItem.imageFile=base64;
                                                                        callback();


                                                                    });
                                                                }
                                                                else{
                                                                     callback();
                                                                 }
                                                            });
                                                        
                                                    } , function(err){
                                                     item.submenu=wareTypes.chunk(3);
                                                     callback();
                                            // if any of the saves produced an error, err would equal that error
                                        });

                                     }
                                });
                           
                        },function(err){
                            res.json(menu); 
                        });    
               
    
};
Array.prototype.chunk = function(chunkSize) {
    var array=this;
    return [].concat.apply([],
        array.map(function(elem,i) {
            return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
        })
    );
}


