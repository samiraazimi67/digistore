require('./../models/ware.js');
var mongoose = require('mongoose');
var conn=mongoose.createConnection('mongodb://shoplogin:hosein2313@localhost:27017/shop');
var ware=conn.model('ware');
var warelist=conn.model('ware');
var wareType=mongoose.model('wareType');
var moment = require('moment-jalaali');
var deepPopulate = require('mongoose-deep-populate');
var fs = require('fs');
var path = require('path');
var Grid = require('gridfs-stream');
var async = require('async');
var ObjectId = require('mongoose').Types.ObjectId;
//var easyimg = require('easyimage');
var moment = require('moment-jalaali');
//var db = require('./../db');
var im = require('imagemagick-stream');

Grid.mongo = mongoose.mongo;
exports.wareTypeList = function (req, res) {
    var fl = req.body.filter;
    for (var key in fl)
    {
        if (fl[key] == null || fl[key] == '')
            delete fl[key];

        if (typeof fl[key] == 'string')
            fl[key] = new RegExp(fl[key]);
    }
    
    
    wareType.count(fl, function (err, result) {
        var ct = result;
        wareType.find(fl).sort({'name': -1}).skip(req.body.skip).limit(req.body.limit)
                .exec(function (err, wareType) {
                    if (err || !wareType) {
                        res.json(err);
                    } else {
                        res.json({data: wareType, count: ct});
                    }
                    ;
                });
    });
};


exports.wareTypeCreate = function (req, res) {
    var reqBody = req.body.info;
    var newWareType = new wareType(reqBody);
    var fileName = req.body.Image;
    var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
    
    var gfs = Grid(conn.db);
    
    var writestream = gfs.createWriteStream({
                filename: fileName + '.1'
    });
   fs.stat(filePath, function (err, found) {
         if (err == null) {
             fs.createReadStream(filePath).pipe(writestream);
             writestream.on('close', function (file) {
                newWareType.image=file._id;
                newWareType.save(function (err, wareType) {
                    if (err || !wareType) {
                        res.json(err);
                     }
                      else {
                         res.json(wareType);
                      }
                });
              });
         }
      else {
          newWareType.save(function (err, wareType) {
           if (err || !wareType) {
                        res.json(err);
                     }
                      else {
                         res.json(wareType);
                      }
          
            });
      }
       
  });
};
       
exports.wareTypeEdit = function (req,res) {
    var reqBody = req.body.info;
    var id = reqBody._id;
    var fileName = req.body.Image;
    var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
    wareType.findOne({_id: id}, function (err, wareTypeEdit) {
        if (err || !wareTypeEdit) {
            res.json(err);
        } else {
            wareTypeEdit.set(reqBody);
            
            var fileName = req.body.Image;
            var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1' );
            var gfs = Grid(conn.db);
            var writestream = gfs.createWriteStream({
                        filename: fileName + '.1'
                    });
           fs.stat(filePath, function (err, found) {
                 if (err == null) {
                     fs.createReadStream(filePath).pipe(writestream);
                     writestream.on('close', function (file) {
                        wareTypeEdit.image=file._id;
                        wareTypeEdit.save(function (err, wareTypeSave) {
                            if (err || !wareTypeSave) {
                                res.json(err);
                             }
                              else {
                                 res.json(wareTypeSave);
                              }
                        });
                      });
                 }
              else {
                  wareTypeEdit.save(function (err, wareTypeSave) {
                            if (err || !wareTypeSave) {
                                res.json(err);
                             }
                              else {
                                 res.json(wareTypeSave);
                              }
                        });
              }

          });
        }
    });
};

exports.wareTypeRead = function (req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    var image={};
    wareType.findOne({_id: id})
            .exec(function (err, docs) {
                var imageId = docs.image;
                if (imageId != undefined) {
                    var bufs = [];
                    gfs.exist({_id: imageId}, function (err, found) {
                        if (found) {
                            var readstream = gfs.createReadStream({
                                _id: imageId
                            });
            
                            readstream.on('data', function (chunk) {
                                bufs.push(chunk);
                            }).on('end', function () {
                                var fbuf = Buffer.concat(bufs);
                                var base64 = (fbuf.toString('base64'));
                                image = base64;
                                if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                            });
                        }
                        else {
                           image = 'undefined';
                            if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                        }
                    });
                }
                else {
                    if (err || !docs) {
                                    res.json(err);
                                } else {
                                    res.json({info:docs,image:image});
                                }
                }
                
            });
};
exports.wareTypeDeleteImage = function (req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    var image={};
    wareType.findOne({_id: id}, function (err, doc) {
                var imageId = doc.image;
                
                if (imageId != undefined) {
                    var bufs = [];
                    gfs.exist({_id: imageId}, function (err, found) {
                        if (found) {
                            var readstream = gfs.createReadStream({
                                _id: imageId
                            });
                            gfs.remove({
                                _id: imageId
                            }, function (err) {
                              if (err) return handleError(err);
                              doc.image=undefined;
                              doc.save(function (err, result) {
                                if (err || !result) {
                                    res.json(err);
                                } else {
                                    res.json({result:result});
                                }
                                });
                            });
                           
                        }
                        else {
                           doc.image=undefined;
                           doc.save(function (err, result) {
                                if (err || !result) {
                                    res.json(err);
                                } else {
                                    res.json({result:result});
                                }
                                });
                        }
                    });
                }
                else {
                   if (err || !doc) {
                                    res.json(err);
                                } else {
                                    res.json({result:doc});
                                }
                }
                
            });
};

exports.wareTypeDelete = function (req, res) {
    var id = req.body.id;
    wareType.findOne({_id: id}, function (err, item) {
                                if (err || !item) {
                                        res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
                                } else {
                                        item.remove(function (err, result) {
                                                if (err) {
                                                        res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
                                                } else {
                                                        res.send({status: 'info', message: 'داده  انتخاب شده با موفقیت حذف گردید.'});
                                                }
                                        });
                                }
                        });
};
exports.create = function (req, res) {
    var reqBody = req.body;
    var newWare = new ware(reqBody);
    newWare.trCreate=moment(Date.now()).format('jYYYYjMMjDD.HHmmss');
      newWare.save(function (err, ware) {
           if (err || !ware) {
                        res.json(err);
                     }
                      else {
                         res.json(ware);
                      }
          
            });
      };
exports.edit = function (req, res) {
    var reqBody = req.body;
    delete reqBody['picFile'];
    delete reqBody['supplier'];
    var id = reqBody._id;
    ware.findOne({_id: id}, function (err, wareEdit) {
        if (err || !wareEdit) {
            res.json(err);
        } else {
            wareEdit.set(reqBody);
            wareEdit.save(function (err, wareSave) {
                if (err || !wareSave) {
                    res.json(err);
                 }
                 else {
                     res.json(wareSave);
                 }
            });
              

        }
    });
};
exports.read = function (req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    ware.findOne({_id: id})
            .populate('supplier')
            .exec(function (err, docs) {
         if (err || !docs) {
                        res.json(err);
                     } else {
                        async.each(docs.pic, function(item, callback) {
                       var imageId=item;
                            console.log(imageId);
                        if (imageId != undefined) {
                            var bufs = [];
                            console.log(imageId);
                            gfs.exist({_id: imageId}, function (err, found) {
                                if (found) {
                                    console.log('found');
                                    var readstream = gfs.createReadStream({
                                        _id: imageId
                                    });

                                    readstream.on('data', function (chunk) {
                                        bufs.push(chunk);
                                    }).on('end', function () {
                                        var fbuf = Buffer.concat(bufs);
                                        var base64 = (fbuf.toString('base64'));
                                        
                                        docs.picFile.push({'id':imageId,'value':base64});
                                        callback();
                                    });
                                }
                                else{
                                    console.log('notfound');
                                   callback(); 
                                }
                           });
                        }
                        else
                            callback(); 
                        
                    },function(err){
                            res.json(docs);
                                
                    });
                         //res.json(docs);
                   
                }
            });
};
exports.similarWare = function(req, res) {
    var id = req.body.id;
    var gfs = Grid(conn.db);
    ware.findOne({_id:id}, function(err,ware) {
        if(err || !ware)
            res.json(err);
        else {
            var minbet=ware.price-20000;
            var maxbet=ware.price+20000;
            warelist.find({wareType:ware.wareType,categories:ware.categories,_id:{'$ne':id}}).where('price').gte(minbet).lte(maxbet).limit(4)
            .populate('supplier')
            .exec(function (err, items) {
                    if (err || !items) {
                        res.json(err);
                    } else {
                        async.each(items,  
                            function(item, callback){
                                if(item.pic.length >0)
                                {
                                    var imageId = item.pic[0];
                                    var bufs = [];
                                    gfs.exist({_id: imageId}, function (err, found) {
                                        if (found) {
                                            var readstream = gfs.createReadStream({
                                                _id: imageId
                                            });

                                            readstream.on('data', function (chunk) {
                                                bufs.push(chunk);
                                            }).on('end', function () {
                                                var fbuf = Buffer.concat(bufs);
                                                var base64 = (fbuf.toString('base64'));

                                                item.picFile=base64;
                                                callback();


                                            });
                                        }
                                    });
                                }
                                else{
                                     callback();
                                 }
                            } , function(err){
                                res.json(items);
                    // if any of the saves produced an error, err would equal that error
                            });
                    };
                });
        }
    })
}
/*exports.addImage =function(req, res) {
    var id = req.body._id;
    var fileName = 'test.jpg';

    ware.findOne({_id: id}, function (err, ware) {
        if (err || !ware) {
            res.json(err);
        } else {

            var gfs = Grid(conn.db);
            var filePath = path.join(__dirname, '..', 'tmp', 'test.jpg' );
            lwip.open(filePath, function(err, image){
                // check 'err'. use 'image'.
                if (err) {
                    res.json(err);
                } else {
                   image.resize(300, 300, function(err,rzdImg){
                       rzdImg.writeFile(filePath, function(err) {
                        if (err) res.json(err);
                        });
                   });
                }
                // image.resize(...), etc.
            });

        var writestream = gfs.createWriteStream({
                filename: fileName 
            });
           
            fs.createReadStream(filePath).pipe(writestream);
            writestream.on('close', function (file) {
                //var image = new Image()
                ware.pic.push(file._id);

                ware.save(function (err, item) {
                    if (err || !item) {
                        res.json(err);
                    } else {
                        res.json(item);
                    }
                });

            });

        }
    });
};*/
/*exports.addImage =function(req, res) {
    var id = req.body._id;
    var fileName = req.body.fileName;

    ware.findOne({_id: id}, function (err, ware) {
        if (err || !ware) {
            res.json(err);
        } else {

            //var gfs = Grid(conn.db);
            //var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1');
            var b64string = req.body.fileName.substr(23);
            //var buf = new Buffer(b64string, 'base64');
            //console.log(buf);
            /*gfs.put(buf,function(err, file){
                res.json(file);
            });
            ware.pic.push(b64string);
            ware.save(function (err, item) {
                    if (err || !item) {
                        res.json(err);
                    } else {
                        res.json({status:'success'});
                    }
                });

        }
    });
};*/
exports.addImage =function(req, res) {
    var id = req.body._id;
    var fileName = req.body.fileName;

    ware.findOne({_id: id}, function (err, ware) {
        if (err || !ware) {
            res.json(err);
        } else {

            var gfs = Grid(conn.db);
            var filePath = path.join(__dirname, '..', 'tmp', 'flow-' + fileName + '.1');

        var writestream = gfs.createWriteStream({
                filename: fileName + '.1'
            });
            var resize = im().resize('300x').quality(80);
            fs.createReadStream(filePath).pipe(resize).pipe(writestream);
            writestream.on('close', function (file) {
                //var image = new Image()
                ware.pic.push(file._id);

                ware.save(function (err, item) {
                    if (err || !item) {
                        res.json(err);
                    } else {
                        res.json({status:'success'});
                    }
                });

            });

        }
    });
};
exports.deleteImage= function(req, res) {
    var imageId=req.body.imageId;
    var id= req.body.id;
    var gfs = Grid(conn.db);
    ware.findOne({_id: id}, function (err, doc) {

                if (imageId != undefined) {
                    var bufs = [];
                    gfs.exist({_id: imageId}, function (err, found) {
                        if (found) {
                            var readstream = gfs.createReadStream({
                                _id: imageId
                            });
                            gfs.remove({
                                _id: imageId
                            }, function (err) {
                              if (err) return handleError(err);
                                
                                var i=doc.pic.indexOf(imageId);
                                doc.pic.splice(i, 1);
                                doc.save(function (err, result) {
                                    if (err || !result) {
                                        res.json(err);
                                    } else {
                                        res.json(result);
                                    }
                                    });
                            });
                           
                        }
                        else {
                           var i=doc.pic.indexOf(imageId);
                            doc.pic.splice(i, 1);
                           doc.save(function (err, result) {
                                if (err || !result) {
                                    res.json(err);
                                } else {
                                    res.json(result);
                                }
                                });
                        }
                    });
                }
                else {
                   if (err || !doc) {
                                    res.json(err);
                                } else {
                                    res.json(doc);
                                }
                }
                
            });
    
}
exports.list= function(req, res){
    var fl = req.body.filter;
    var skip=req.body.skip;
    var count=req.body.count;
    var gfs = Grid(conn.db);
    var min=0;
    var max=999999999999;
    for (var key in fl)
    {
        //console.log(typeof fl[key]);
        if ((fl[key] == null || fl[key] == '') && fl[key] != "0" )
            delete fl[key];

     /*   if (typeof fl[key] == 'string')
            fl[key] = new RegExp(fl[key]);
       */
        if( key=='maxPrice')
        {
            max=fl[key];
            delete fl[key];
        }
        if( key == 'minPrice')
        {
            min=fl[key];
            delete fl[key];
        }
        if( key == 'off')
        {
            fl['off']={$gt:fl['off']};
        }
        if (key == 'tag')
        {
            var arraytag = fl[key].split(' ');
            for(var i=0;i<arraytag.length;i++)
            {
                
                arraytag[i]=new RegExp(arraytag[i]);
            }
            fl['tag']={$all:arraytag};
            
        }
    }
    console.log(fl);
  ware.find(fl)
        .where('price').gte(min).lte(max)
        .skip(skip)
        .limit(count)
        .sort(req.body.orderBy)
       .populate('supplier')
       .exec(function (err, items) {
            console.log(items);
             if (err || !items) {
                res.json(err);
             } else {
                 async.each(items,  
                            function(item, callback){
                                if(item.pic.length >0)
                                {
                                    var imageId = item.pic[0];
                                    var bufs = [];
                                    gfs.exist({_id: imageId}, function (err, found) {
                                        if (found) {
                                            var readstream = gfs.createReadStream({
                                                _id: imageId
                                            });

                                            readstream.on('data', function (chunk) {
                                                bufs.push(chunk);
                                            }).on('end', function () {
                                                var fbuf = Buffer.concat(bufs);
                                                var base64 = (fbuf.toString('base64'));

                                                item.picFile=base64;
                                                callback();


                                            });
                                        }
                                    });
                                } 
                                 else{
                                     callback();
                                 }
                            } , function(err){
                                res.json(items);
                    // if any of the saves produced an error, err would equal that error
                            });
               
             }
        });
};
exports.delete = function (req, res) {
    var id = req.body.id;
    ware.findOne({_id: id}, function (err, item) {
                                if (err || !item) {
                                        res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
                                } else {
                                        item.remove(function (err, result) {
                                                if (err) {
                                                        res.send({status: 'error', message: 'خطا در برقراری ارتباط با سرور'});
                                                } else {
                                                        res.send({status: 'info', message: ';کالای  انتخاب شده با موفقیت حذف گردید.'});
                                                }
                                        });
                                }
                        });
};
exports.existsChange = function (req, res) {
    var id = req.body.id;
    ware.findOne({_id: id}, function (err, wareEdit) {
        if (err || !wareEdit) {
            res.json(err);
        } else {
            if(wareEdit.exists)
                wareEdit.exists=false;
            else
                wareEdit.exists=true;
            wareEdit.save(function (err, wareSave) {
                if (err || !wareSave) {
                    res.json(err);
                 }
                 else {
                     res.json(wareSave);
                 }
            });
              

        }
    });
};