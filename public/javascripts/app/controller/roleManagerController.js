var app = angular.module('myApp');
app.controller('roleManagerController', function( $scope,$http,$modal ,$stateParams,ngTableParams,$location) {
	$scope.roleName = $stateParams.roleName;
	$scope.rolesTable = new ngTableParams({
		page: 1,
		count: 10
	}, {
		counts: [],
		total: 5, // length of data
		filterDelay: 50,
		getData: function ($defer, params) {
			$http.get("role/list").success(function (data) {	
				
				$scope.form = data;
				$defer.resolve(data);
				
			});
		}
	});
	

	$scope.roleEditInline = function(role){
		$http.post("role/update",role)
			.success(function (data) {	
			role.$edit = false;
		});
	};	

	$scope.rolePermissionsTable = new ngTableParams({
		page: 1,
		count: 10
	}, {
		counts: [],
		total: 5, // length of data
		filterDelay: 50,
		getData: function ($defer, params) {
			$http.post("role/permissions",{roleName:$stateParams.roleName}).success(function (data) {	
				console.log($stateParams.roleName)
				$scope.form = data;
				$defer.resolve(data);
				
			});
		}
	});
		
	$scope.PermissionsTable = new ngTableParams({
		page: 1,
		count: 10
	}, {
		counts: [],
		total: 5, // length of data
		filterDelay: 50,
		getData: function ($defer, params) {
			$http.post("permission/list").success(function (data) {	
				$scope.form = data;
				$defer.resolve(data);
				
			});
		}
	});
	$scope.permissionAddModal = function (size, id) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/admin/RoleAddPermission.html',
			controller: 'permissionAddController',
			size: size,
			resolve: {
				roleIdToAdd : function() { return id;}
			}
			
		});
		
		modalInstance.result.then(
			function (result) {
				$scope.rolesTable.reload();
			}, function () {
				$scope.rolesTable.reload();
			}
		);
	};

	$scope.roleCreate = function(name){
		$http.post("role/create",{name:name})
			.success(function (data) {	
			$scope.rolesTable.reload();
		});
	};	
	$scope.roleDelete = function(id){
		$http.post("role/delete",{id:id})
			.success(function (data) {	
			$scope.rolesTable.reload();
		});
	};	
	
	$scope.roleRemovePermission = function(roleName,id){
		$http.post("role/removePermission",{roleName:roleName,permissionId:id})
			.success(function (data) {	
			$scope.rolesTable.reload();
		});
	};

	
});
app.controller('permissionAddController', function( roleIdToAdd,$scope,$http,$modal ,$stateParams,ngTableParams,$location) {
	$scope.roleIdToAdd = roleIdToAdd;
		
	$scope.PermissionsTable = new ngTableParams({
		page: 1,
		count: 10
	}, {
		counts: [],
		total: 5, // length of data
		filterDelay: 50,
		getData: function ($defer, params) {
			$http.post("permission/list").success(function (data) {	
				$scope.form = data;
				$defer.resolve(data);
				
			});
		}
	});
	$scope.permissionEditInline = function(permission){
		$http.post("permission/update",permission)
			.success(function (data) {	
			permission.$edit = false;
		});
	};		
	

	$scope.permissionCreate = function(subject,action){
		$http.post("permission/create",{subject:subject,action:action})
			.success(function (data) {	
			$scope.PermissionsTable.reload();
		});
	};	
	$scope.permissionDelete = function(id){
		$http.post("permission/delete",{id:id})
			.success(function (data) {	
			$scope.PermissionsTable.reload();
		});
	};	

	$scope.roleAddPermission = function(roleIdToAdd,id){
		$http.post("role/addPermission",{roleIdToAdd:roleIdToAdd,permissionId:id})
			.success(function (data) {	
			
		});
	};	
	
	
});



