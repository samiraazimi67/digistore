var app = angular.module('myApp');
app.controller('shopController', function( $scope, $rootScope, $filter,$modal, $location,$http, $window, $stateParams, ngTableParams) {
$scope.shopList = {};
$scope.shop={};
$scope.image={};
$scope.init = function () {
        if ($stateParams.id != undefined) {
            $http.post('/shop/read',{id: $stateParams.id }).
				success(function(data){
					$scope.shop = data.info ;
                    $scope.image=data.image;
				});
        }
		
};
$scope.loginInit=function(){
    $rootScope.isAdminPage=true;
    $scope.logout();
}	
$scope.adminInit= function() {
    $rootScope.isAdminPage=true;
    $scope.searchWare = {};
};
    // shoplist for customer view. list has pic base64

$scope.filter = {name: '',tel:'' ,address: ''};

$scope.list = new ngTableParams({
        page: 1,
        count: 10,
        filter: $scope.filter
    }, {
        counts: [],
        total: $scope.shopList.length, // length of data
        filterDelay: 50,
        getData: function ($defer, params) {

            var fl = {'name': params.filter().name,
                      'tel': params.filter().tel,
                      'clientName':params.filter().clientName
            };


            $http.post("shop/listTable", {skip: (params.page() - 1) * params.count(), limit: params.count(), filter: fl, orderBy: {'name': -1}})
                    .success(function (result) {
                        $scope.shopList = result.data;
                        var orderedData = params.sorting() ?
                               
                                $filter('orderBy')($scope.shopList, params.orderBy()) :
                                $scope.shopList;

                        /*	orderedData = params.filter() ?
                         $filter('filter')(orderedData, params.filter()) :
                         orderedData;
                         */
                        params.total(result.count); // set total for recalc pagination
                        $defer.resolve(orderedData);

                    });
        }
    });  
    $scope.createShop = function(imageIdentify){
			$scope.loading = true;
			$http.post('/shop/create', {'info': $scope.shop,'Image':imageIdentify}).
				success(function(result) {
                $scope.loading = false;
					if  (result == 'undefined')
					{
						//$location.path('/admin/createShop');
						$rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
					}
					else
					{	
                        if(result=='repeatUsername')
                        {
                            $rootScope.runNotify('error', 'این نام کاربری قبلا استفاده شده است. لطفا نام کاربری جدیدی انتخاب کنید');
                        }
                        else{
                            $location.path('/login');
                            $rootScope.runNotify('success', 'فروشگاه جدیدی با موفقیت ثبت شد. لطفا جهت ورود به فروشگاه خود نام کاربری و رمز عبور را وارد کنید');
                        }
					}
				});
		
	};
    $scope.editShop = function(imageIdentify){
	
		$http.post('/shop/edit', {'info': $scope.shop,'Image':imageIdentify}).
				success(function(data) {
					
					//$location.path('/admin/adminShops');
					$rootScope.runNotify('success', ' ویرایش فروشگاه با موفقیت ذخیره شد.');
				});
		
	};
    
    $scope.deleteImage = function (id) {
        var options = {
            id: id,
        };
        $http.post('/shop/deleteImage', options).
                success(function (data) {
                        $rootScope.runNotify('success', 'تصویر با موفقیت پاک شد.');
                        $scope.image=data.result.image;
                                    
                });

    };
    /////////////// login
        $scope.login = function (isValid) {
               if (isValid && navigator.appVersion.indexOf('MSIE') === -1) {
                        $http.post('/login', $scope.form).
                                success(function (data) {
                                        if (data.status === "success") {
                                                $rootScope.me = data.user;
                                                $rootScope.auth=data.auth;
                                                $rootScope.jDate = data.jDate;
                                                $location.path('/admin');
                                                $rootScope.runNotify('success', 'ورود با موفقیت انجام شد.');
                                                
                                        }
                                        else {
                                                $rootScope.runNotify('error', data.message);
                                        }
                                });
                }
                else {
                        $rootScope.runNotify('warning', 'لطفا از مرورگر استاندارد مانند Chrome یا FireFox استفاده نمایید.');
                }
        };
        $scope.logout = function logout() {
                $http.post('/logout').
                        success(function (data) {
                                $rootScope.me = {};
                                $rootScope.auth=false;
                                $rootScope.vprofileVisible = false;
                               
                                $location.path('/login');
                        });
        };
        $scope.shopRoleInit= function(){
            $scope.shopId=
            $http.get("role/list").success(function (data) {	
				$scope.roles = data;
			});
        };
        $scope.memberAddRole = function () {
                console.log($scope.form);
                $http.post("shop/addRole", {_id: $stateParams.id, roleId: $scope.form.role_id})
                        .success(function (data) {
                                $scope.rolesTable.reload();
                        });
        };
        $scope.removeRole = function (roleId) {
                $http.post("shop/removeRole", {_id: $stateParams.id, roleId: roleId})
                        .success(function (data) {
                                $scope.rolesTable.reload();
                        });
        };
    $scope.rolesTable = new ngTableParams({
                page: 1,
                count: 10
        }, {
                counts: [],
                total: 5, // length of data
                filterDelay: 50,
                getData: function ($defer, params) {
                    
                    console.log($stateParams.id);
                        $http.post("shop/roleList",{_id: $stateParams.id}).success(function (data) {


                                $defer.resolve(data);
                        });
                }
        });
    
    $scope.openShopDeleteModal = function (controllerName,deleteParameter) {
                var modalInstance = $modal.open({
                        templateUrl: 'deleteModal.html',
                        controller: controllerName,
                        resolve: {
                         deleteParameter: function () {
                           return deleteParameter;
                         }
                       }
                        
                });

               modalInstance.result.then(function (result) {
                        $scope.list.reload();
                }, function () {
                        $scope.list.reload();
                });
        };
});
