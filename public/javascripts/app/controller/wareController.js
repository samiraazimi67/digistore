// in search ware, do search on or just tag or selected shop or waretyp and....
var app = angular.module('myApp');
app.controller('wareController',function( $scope, $rootScope, $filter, $location,$modal,$http, $window, $stateParams,$state , ngTableParams,ImageResizerService) {
$scope.wareType={};
$scope.shopId=-1; 
$scope.ware = {};
$scope.saleWareList={};
$scope.catId =-1;
$scope.catName= null;
$scope.searchWare = {};
$scope.shopList={};
$scope.waretypes={};
$scope.filterTag={};
$scope.searchTag={};
$scope.sortKey={key:{'trCreate':-1}};
$scope.sortArray=[{key:{'trCreate':-1},label:'جدیدترین'},{key:{'price':-1},label:'گرانترین'},{key:{'price':1},label:'ارزانترین'},
                  {key:{'off':-1},label:'بالاترین حراج'}];
$scope.resize = function () {
    ImageResizerService.resizeImage('#imgTumb');
};
$scope.ware.color=[];
    $scope.mainHomeinit= function(){
        var fl={'off':1};
        $http.post('/ware/list',{filter:fl,orderBy:{'off':-1}}).
                    success(function(result){
                        $scope.saleWareList=result;
                });
    };
$scope.removeFilter=function(searchKey) {
     delete $scope.searchWare[searchKey];
    $scope.DosearchWare();
    $scope.updateSearchTextBox();
    
};
$scope.removeAdminFilter=function(searchKey) {
    if(searchKey != 'supplier')
    {
        delete $scope.searchWare[searchKey];
        $scope.DosearchWare();
    }
    else{
        $rootScope.runNotify('error', 'در صفحه شخصی شما فقط مجاز به تغییر کالای خود می باشید');
    }
   
};
$scope.wareInit = function () {
        if ($stateParams.supplier != undefined) {
            $scope.shopId=$stateParams.supplier;
        }
        if ($stateParams.id != undefined) {
            $http.post('/ware/read',{id: $stateParams.id }).
				success(function(result){
					$scope.ware = result ;
                   });
        }
    /*$http.post('/wareType/list').
				success(function(result){
					$scope.wareTypeList = result.data ;
				});*/
	};
$scope.wareReadInit = function () {
    if ($stateParams.id != undefined) {
            $http.post('/ware/read',{id: $stateParams.id }).
				success(function(result){
					$scope.ware = result ;
				});
            $http.post('/ware/similarWare',{id:$stateParams.id}).
                success(function(result){
                    $scope.similarWare= result;
                });
        }
    
};
$scope.customerHomeInit= function() {
   $rootScope.isAdminPage=false;
   /* $http.post('/wareType/list').
				success(function(result){
					$scope.wareTypeList = result.data ;
				});
    */
};
$scope.loadMoreWare = function() {
           // var last = $scope.wareList[$scope.wareList.length - 1];
            $scope.isLoading = true;
            $http.post('/ware/list',{filter:$scope.searchWare,skip:$scope.wareList.length,count:12,orderBy:$scope.sortKey.key}).
                    success(function(result){
                    for (i=0; i < result.length;i++)
                    {
                        $scope.wareList.push(result[i]);
                        
                    }
                    $scope.isLoading = false;
                });
            
          };
$scope.wareListInit = function() {
    if($stateParams.supplier != undefined)
    {
            $scope.shopId=$stateParams.supplier;
            $scope.searchWare['supplier']=$stateParams.supplier;
        
    }
    if($stateParams.tag != undefined)
    {
            $scope.searchWare={'tag':$stateParams.tag};
        
    }
    $http.post('/wareType/list').
				success(function(result){
					$scope.waretypes = result.data ;
				});
    $scope.updateTag();
    $http.post('/ware/list',{filter:$scope.searchWare,skip:0,count:12,orderBy:$scope.sortKey.key}).
                success(function(result){
                    $scope.wareList=result;
            });
};
    
//for shop/shoList customer. this list has shop imagefile field (with base64)
$scope.shopListInit = function() {
   $http.post('/shop/list',{skip:0,count:12,orderBy:{off:-1}}).
                success(function(result){
                    $scope.shopList=result;
            });
};
$scope.loadMoreShop = function() {
   // var last = $scope.wareList[$scope.wareList.length - 1];
    $scope.isLoading = true;
    $http.post('/shop/list',{skip:$scope.shopList.length,count:12,orderBy:{off:-1}}).
        success(function(result){
            for (i=0; i < result.length;i++)
            {
                $scope.shopList.push(result[i]);
            }
                $scope.isLoading = false;
        });
};
$scope.changeCat= function(catObject) {
    $scope.searchWare={};
    if(catObject.wtId !=undefined)
        $scope.searchWare['wareType']=catObject.wtId;
    if(catObject.catId !=undefined)
        $scope.searchWare['categories']=catObject.catId;
    
    delete $scope.searchWare['tag'];
    $scope.DosearchWare();
    //update search tag textbox 
    $scope.updateSearchTextBox();
    
    
}
$scope.changeShop= function(shopId) {
    $scope.searchWare={};
    $scope.searchWare['supplier']=shopId;
    delete $scope.searchWare['tag'];
    $scope.DosearchWare();
    $scope.updateSearchTextBox();
    
}
$scope.searchInTag= function() {
    if($scope.searchTag.text != undefined && $scope.searchTag.text.length !=0)
        $scope.searchWare={'tag':$scope.searchTag.text};
    else
        $scope.searchWare={};
    $scope.DosearchWare();
    
}
$scope.DosearchWare= function(){
    $rootScope.searchMenuVisible=false;
        $scope.updateTag();
        $http.post('/ware/list',{filter:$scope.searchWare,skip:0,count:12,orderBy:$scope.sortKey.key}).
            success(function(result){
                $scope.wareList=result;
        });
    };
$scope.updateTag= function(){
    $scope.filterTag=angular.copy($scope.searchWare);
    if($scope.filterTag['tag'] != undefined)
        $scope.filterTag['tag']={'val':$scope.searchWare['tag'],'label':''};
    if($scope.filterTag['code'] != undefined)
        $scope.filterTag['code']={'val':$scope.searchWare['code'],'label':'کد:'};
    if($scope.filterTag['material'] != undefined)
        $scope.filterTag['material']={'val':$scope.searchWare['material'],'label':''};
    if($scope.filterTag['off'] != undefined)
        $scope.filterTag['off']={'val':$scope.searchWare['off'],'label':'حراج بالای'};
    if($scope.filterTag['maxPrice'] != undefined)
        $scope.filterTag['maxPrice']={'val':$scope.searchWare['maxPrice'],'label':'قیمت تا'};
    if($scope.filterTag['minPrice'] != undefined)
        $scope.filterTag['minPrice']={'val':$scope.searchWare['minPrice'],'label':'قیمت از'};
    if($scope.filterTag['size'] != undefined)
        $scope.filterTag['size']={'val':$scope.searchWare['size'],'label':'سایز:'};
    if($scope.filterTag['mark'] != undefined)
        $scope.filterTag['mark']={'val':$scope.searchWare['mark'],'label':'مارک:'};
    if($scope.filterTag['mark'] != undefined)
        $scope.filterTag['mark']={'val':$scope.searchWare['mark'],'label':'مارک:'};
    if($scope.filterTag['categories'] != undefined)
        $scope.filterTag['categories']={'val':$scope.catList[$scope.searchWare['categories']].name,'label':''};
    if($scope.filterTag['supplier'] != undefined)
    {
        var result = $scope.suppliers.filter(function( obj ) {
            return obj._id == $scope.filterTag['supplier'];
        });
        $scope.filterTag['supplier']={'val':result[0].name,'label':'فروشگاه'};
    }
    if($scope.filterTag['wareType'] != undefined)
    {
      
        var result = $rootScope.wareTypeList.filter(function( obj ) {
            return obj._id == $scope.filterTag['wareType'];
        });
        $scope.filterTag['wareType']={'val':result[0].name,'label':''};
    }
        
};
//update textbox when change categoreis or shop or wareType
$scope.updateSearchTextBox=function(){
    $scope.searchTag.text='';
    delete $scope.searchWare['tag'];
    if($scope.searchWare['categories'] != undefined)
        $scope.searchTag.text=$scope.catList[$scope.searchWare['categories']].name;
    if($scope.searchWare['supplier'] != undefined)
    {
        var result = $scope.suppliers.filter(function( obj ) {
            return obj._id == $scope.searchWare['supplier'];
        });
        $scope.searchTag.text=$scope.searchTag.text+' '+result[0].name;
    }
    if($scope.searchWare['wareType'] != undefined)
    {
      
        var result = $rootScope.wareTypeList.filter(function( obj ) {
            return obj._id == $scope.searchWare['wareType'];
        });
        $scope.searchTag.text=$scope.searchTag.text+' '+result[0].name;
    
    }
}
 $scope.wareListReload= function(){
     if($stateParams.supplier != undefined)
    {
        if ($stateParams.supplier != undefined) {
            //$scope.shopId=$stateParams.supplier;
            $scope.searchWare['supplier']=$stateParams.supplier;
        }
        
    }
     $http.post('/ware/list',{filter:$scope.searchWare,skip:0,count:12,orderBy:$scope.sortKey.key}).
        success(function(result){
            $scope.wareList=result;
    });
 };   
$scope.wareTypeInit = function () {
        if ($stateParams.id != undefined) {
            $http.post('/wareType/read',{id: $stateParams.id }).
				success(function(result){
                    $scope.wareType = result.info ;
                    $scope.image=result.image;
				});
        }
		
			
	};
$scope.filter = {name: ''};

$scope.wareTypeListTable = new ngTableParams({
        page: 1,
        count: 10,
        filter: $scope.filter
    }, {
        counts: [],
        total: $scope.waretypes.length, // length of data
        filterDelay: 50,
        getData: function ($defer, params) {

            var fl = {'name': params.filter().name};


            $http.post("wareType/list", {skip: (params.page() - 1) * params.count(), limit: params.count(), filter: fl, orderBy: {'name': -1}})
                    .success(function (result) {
                        ;
                        $scope.waretypes = result.data;
                        var orderedData = params.sorting() ?
                               
                                $filter('orderBy')($scope.waretypes, params.orderBy()) :
                                $scope.waretypes;

                        /*	orderedData = params.filter() ?
                         $filter('filter')(orderedData, params.filter()) :
                         orderedData;
                         */
                        params.total(result.count); // set total for recalc pagination
                        $defer.resolve(orderedData);

                    });
        }
    });  
    $scope.createWareType = function(imageIdentify){
			$http.post('/wareType/create', {'info': $scope.wareType,'Image':imageIdentify}).
				success(function(result) {
					if  (result == 'undefined')
					{
						$location.path('/admin/wareType/create');
						$rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
					}
					else
					{	
						$location.path('/admin/wareType/list');
						$rootScope.runNotify('success', 'نوع جدید با موفقیت ثبت شد');
					}
				});
		
	};
    $scope.editWareType = function(imageIdentify){
        
       /* for (i=1;i<$scope.wareType.categories.length;i++)
        {
           if($scope.wareType.categories[i]===false)
               $scope.wareType.categories.splice(i, 1);
        }*/
        
		$http.post('/wareType/edit', {'info': $scope.wareType,'Image':imageIdentify} ).
				success(function(result) {
					if  (result == 'undefined')
					{
						$location.path('/admin/wareType/edit');
						$rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
					}
					else
					{	
						$location.path('/admin/wareType/list');
						$rootScope.runNotify('success', 'نوع جدید با موفقیت ثبت شد');
					}
				});
        
		
	};
    $scope.createWare = function(){
        if ($stateParams.supplier != undefined) {
            $scope.shopId=$stateParams.supplier;
        }
        $scope.ware.tag=[];
        if($scope.ware.size != undefined)
        {
            $scope.ware.size = $scope.ware.size.map(function(tag) { return tag.text; });
            $scope.ware.tag.push($scope.ware.size);
        }
        $scope.ware.color = $.map( $scope.ware.color, function( value, key ) {return value;});
        $scope.ware.supplier=$scope.shopId;
        $scope.ware.tag.push($scope.ware.mark);
        $scope.ware.tag.push($scope.ware.code);
        $scope.ware.tag.push($scope.catList[$scope.ware.categories].name);
        var result = $scope.suppliers.filter(function( obj ) {
            return obj._id == $scope.ware.supplier;
        });
        $scope.ware.tag.push(result[0].name);
        var result = $rootScope.wareTypeList.filter(function( obj ) {
            return obj._id == $scope.ware.wareType;
        });
        $scope.ware.tag.push(result[0].name);
		$http.post('/ware/create', $scope.ware).
				success(function(result) {
					if  (result == 'undefined')
					{
						$location.path('/admin/ware/create');
						$rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
					}
					else
					{	
						$location.path('/admin/ware/edit/'+result._id);
						$rootScope.runNotify('success', 'کالای جدید با موفقیت ثبت شد');
					}
				});
		
	};
    $scope.deleteWareTypeImage = function (id) {
        var options = {
            id: id,
        };
        $http.post('/wareType/deleteWareTypeImage', options).
                success(function (data) {
                        $rootScope.runNotify('success', 'تصویر با موفقیت پاک شد.');
                        $scope.image=data.result.image;
                                    
                });

    };
    $scope.editWare = function(){
        $scope.ware.tag=[];
        if($scope.ware.size != undefined)
        {
            $scope.ware.size = $scope.ware.size.map(function(tag) { return tag.text; });
            $scope.ware.tag.push($scope.ware.size);
        }
        $scope.ware.color = $.map( $scope.ware.color, function( value, key ) {return value;});
        $scope.ware.tag.push($scope.ware.mark);
        $scope.ware.tag.push($scope.ware.code);
        $scope.ware.tag.push($scope.catList[$scope.ware.categories].name);
        console.log($scope.ware.supplier);
        var result = $scope.suppliers.filter(function( obj ) {
            return obj._id == $scope.ware.supplier._id;
        });
        console.log(result);
        $scope.ware.tag.push(result[0].name);
        var result = $rootScope.wareTypeList.filter(function( obj ) {
            return obj._id == $scope.ware.wareType;
        });
        $scope.ware.tag.push(result[0].name);
        $http.post('/ware/edit', $scope.ware).
				success(function(result) {
					if  (result == 'undefined')
					{
						$location.path('/admin/ware/edit'+$scope.ware._id);
						$rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
					}
					else
					{	
						$location.path('/admin/ware/edit/'+result._id);
						$rootScope.runNotify('success', 'کالا با موفقیت ویرایش و ثبت شد');
					}
				});
		
	};
    
    $scope.existsChange= function(id)
    {
        $http.post('/ware/existsChange', {id:id}).
				success(function(result) {
                    $scope.wareListReload();
				});
    }
    $scope.addImage = function(ide){
        //$scope.resize();
        //console.log('ok');
        var options = {
            _id: $stateParams.id,
            fileName: ide
        };
        $http.post('/ware/addImage', options).
                success(function (result) {
                    //$window.location.reload();
            if(result.status=='success')
            {
                $state.go($state.current, {}, {reload: true});
                $rootScope.runNotify('success', 'تصویر با موفقیت  ثبت شد.');
            }
            else
                $rootScope.runNotify('error', 'خطا در ثبت اطلاعات');
                    
        });

    };
    $scope.updateTrCreate= function(){
        $scope.ware.trCreate=moment(Date.now()).format('jYYYYjMMjDD.HHmmss');
    }
    $scope.updateWareTypeList= function(wareTypeId)
    {
        $http.post('/wareType/list',{filter: {'categories':wareTypeId}}).
				success(function(result){
					$scope.waretypes = result.data ;
				});
	};
    $scope.openWareTypeDeleteModal = function (controllerName,deleteParameter) {
                var modalInstance = $modal.open({
                        templateUrl: 'deleteModal.html',
                        controller: controllerName,
                        resolve: {
                         deleteParameter: function () {
                           return deleteParameter;
                         }
                       }
                        
                });

               modalInstance.result.then(function (result) {
                        $scope.wareTypeListTable.reload();
                }, function () {
                        $scope.wareTypeListTable.reload();
                });
        };
    $scope.openWareDeleteModal = function (controllerName,deleteParameter) {
                var modalInstance = $modal.open({
                        templateUrl: 'deleteModal.html',
                        controller: controllerName,
                        resolve: {
                         deleteParameter: function () {
                           return deleteParameter;
                         }
                       }
                        
                });

               modalInstance.result.then(function (result) {
                        $scope.wareListReload();
                }, function () {
                        $scope.wareListReload();
                });
        };
    $scope.openWareImageDeleteModal = function (controllerName,deleteParameter) {
                var modalInstance = $modal.open({
                        templateUrl: 'deleteModal.html',
                        controller: controllerName,
                        resolve: {
                         deleteParameter: function () {
                           return deleteParameter;
                         }
                       }
                        
                });

               modalInstance.result.then(function () {
                   //var index=$scope.ware.picFile.indexOf(imageId);
                   //$scope.ware.picFile.splice(index,1);
                   $state.go($state.current, {}, {reload: true});
                }, function () {
                   $state.go($state.current, {}, {reload: true});
                });
        };
      
});
app.controller('wareModalController', function( $scope, $rootScope, $filter, $location,$modal,$http, $window, $stateParams,$state ,$uibModalInstance, ngTableParams,deleteParameter) {
    $scope.deleteParameter=deleteParameter;
    $scope.callFunction = function(name){
        $scope.$eval(name);
    }
    
    $scope.deleteWareType= function(id){
        $http.post('/wareType/delete', {'id': id} ).
				success(function(result) {
					if  (result == 'undefined')
					{
						$rootScope.runNotify('error', 'خطا در حذف اطلاعات');
					}
					else
					{	
                        $uibModalInstance.dismiss('cancel');
						$rootScope.runNotify('success', 'ردیف های مورد نظر با موفقیت حذف شد');
					}
				});
    }
    $scope.deleteWare= function(id){
        $http.post('/ware/delete', {'id': id} ).
				success(function(result) {
					if  (result == 'undefined')
					{
						$rootScope.runNotify('error', 'خطا در حذف اطلاعات');
					}
					else
					{	
                        $uibModalInstance.dismiss('cancel');
						$rootScope.runNotify('success', 'کالای مورد نظر با موفقیت حذف شد');
					}
				});
    }
    $scope.deletewareImage= function(imageId,id){
        $http.post('/ware/deleteImage', {'imageId': imageId,'id':id} ).
				success(function(result) {
					if  (result == 'undefined')
					{
						$rootScope.runNotify('error', 'خطا در حذف اطلاعات');
					}
					else
					{	
                        $uibModalInstance.close();
						$rootScope.runNotify('success', 'تصویر مورد نظر با موفقیت حذف شد');
					}
				});
    }
    $scope.cancel=function (){
        
        $uibModalInstance.dismiss('cancel');
    }
    $scope.deleteShop= function(id){
        $http.post('/shop/delete', {'id': id} ).
				success(function(result) {
					if  (result == 'undefined')
					{
						$rootScope.runNotify('error', 'خطا در حذف اطلاعات');
					}
					else
					{	
                        $uibModalInstance.dismiss('cancel');
						$rootScope.runNotify('success', 'فروشگاه مورد نظر با موفقیت حذف شد');
					}
				});
    }
});