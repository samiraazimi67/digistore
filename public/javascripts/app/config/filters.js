
angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    };
  }]);
  
app.filter('floor', function(){

    return function(n){
        return Math.floor(n);
    };
	});
app.filter('jDate',function(){

    return function(n){
		var	val = Math.floor(n).toString();
		var	res = "9999/99/99".split("");
		var i;
		var j = 0;

		for( var i = 0;i<res.length;i++) 
		{	if (res[i] == '9')
			{
				res[i] = val[j];
				j++;
			}
        }
		return res.join("");
    };
	});
app.filter('jTime',function(){

    return function(n){
		var	val = ((n - Math.floor(n))*1000000).toString() ;
		var	res = "99:99:99".split("");
		var i;
		var j = 0;

		for( var i = 0;i<res.length;i++) 
		{	if (res[i] == '9')
			{
				res[i] = val[j];
				j++;
			}
        }
		return res.join("");
    };
});
app.filter('propsFilter', function () {
	return function (items, props) {
		var out = [];

		if (angular.isArray(items)) {
			items.forEach(function (item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});
