'use strict';
angular.module('vMenu', [])
.directive("menu", function() {
    return {
        restrict: "E",
        template: "<div ng-class='{show: visible, left: alignment === \"left\", right: alignment === \"right\" }' ng-transclude ng-scrollbars ng-scrollbars-config=scrollbars></div>",
        transclude: true,
        scope: {
            visible: "=",
            alignment: "@",
            scrollbars:"="
        }
    };
})

.directive("menuItem", function() {
     return {
         restrict: "E",
         template: "<div ng-transclude></div>",
         transclude: true,
         scope: {
             url: "@"
         },
         
     }
});