'use strict';
angular.module('bootstrap.tabset', [])
	.directive('tabsets', function () {
		return {
			restrict: 'E',
			replace: true,
			transclude: true,
			controller: function($scope) {
				$scope.templateUrl = '';
				var tabs = $scope.tabs = [];
				var controller = this;
				this.selectTab = function (tab) {
					angular.forEach(tabs, function (tab) {
						tab.selected = false;
					});
					tab.selected = true;
				};
				this.setTabTemplate = function (templateUrl) {
					$scope.templateUrl = templateUrl;
				}
				this.addTab = function (tab) {
					if (tabs.length == 0) {
						controller.selectTab(tab);
					}
					tabs.push(tab);
				};
			},
			template:
				'<div class="row-fluid-tab">' +
				'<div class="nav-tabs-custom">' +
				'<div class="nav nav-tabs" ng-transclude></div>' +
				'<div class="tab-content animate">' +
				'<ng-include src="templateUrl" class="animate"></ng-include>' +
				'</div>' +
				'</div>' +
				'</div>'
		};
	})
	.directive('tabs', function () {
		return {
			restrict: 'E',
			replace: true,
			require: '^tabsets',
			scope: {
				title: '@',
				templateUrl: '@'
			},
			link: function(scope, element, attrs, tabsetController) {
				tabsetController.addTab(scope);
				scope.select = function () {
					tabsetController.selectTab(scope);
				}
				scope.$watch('selected', function () {
					if (scope.selected) {
						tabsetController.setTabTemplate(scope.templateUrl);
					}
				});
			},
			template:
				'<li ng-class="{active: selected}">' +
				'<a href="" ng-click="select()">{{ title }}</a>' +
				'</li>'
		};
	});