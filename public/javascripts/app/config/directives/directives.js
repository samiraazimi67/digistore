app.directive('onlyDigits', function () {
	return {
		require: 'ngModel',
		restrict: 'EA',
		link: function (scope, element, attrs, ctrl) {
			function inputValue(val) {
				if(val) {
					var digit = val.replace(/[^0-9]/g, '');

					if(digit !== val) {
						ctrl.$setViewValue(digit);
						ctrl.$render();
					}
					return digit;
				}
				return undefined;
			}
			ctrl.$parsers.push(inputValue);
		}
	};
});
app.directive('onlyRealdigits', function () {
	return {
		require: 'ngModel',
		restrict: 'EA',
		link: function (scope, element, attrs, ctrl) {
			function inputValue(val) {
				if(val) {
					var digit = val.replace(/[^0-9,.]/g, '');

					if(digit !== val) {
						ctrl.$setViewValue(digit);
						ctrl.$render();
					}
					return digit;
				}
				return undefined;
			}
			ctrl.$parsers.push(inputValue);
		}
	};
});

app.directive('tooltip', function($compile) {
	return {
		require: 'ngModel',
		restrict: 'EA',
		link: function (scope, element, attrs, ctrl) {
			element.bind('blur', function() {
				if(ctrl.$invalid) {
					element.parent().removeClass('has-success');
					element.parent().addClass('has-error');
					var errorBlock = element.parent().find('.help-block').html();
					if(typeof errorBlock !== 'undefined') {
						if(typeof element.attr('data-toggle') !== 'undefined') {
							if(element.attr('title') != errorBlock) {
								element.attr('title', errorBlock);
							}						
						}
						else {
							element.attr('data-toggle', "tooltip");
							element.attr('data-placement', "top");
							element.attr('title', errorBlock);
						}
					}
					element.tooltip();
				}
				else {
					element.parent().removeClass('has-error');
					element.parent().addClass('has-success');
					element.removeAttr('data-toggle');
					element.removeAttr('data-placement');
					element.removeAttr('title');
					element.tooltip('destroy')
				}
			});
		}
	};
});

app.directive('loadModal', function($compile) {
	return {
		require: 'ngModel',
		restrict: 'EA',
		link: function (scope, element, attrs, ctrl, $http) {
			
			var modalStyle = '';
			element.bind('click', function() {
			});
		}
	};
});

app.directive('permission', function () {
    return {
        restrict: 'ECA',
        scope: {
            permission: '@'
        },
        controller: function ($scope, $http, AuthenticationService) {
            $scope.getPerms = function (action, subject) {
                AuthenticationService.authenticate(action, subject).success(function (data) {
                    $scope.auth = data.auth;
                });
            };
        },
        link: function (scope, element, attrs, ctrl) {
            var attr = attrs.permission.split(",");
            scope.getPerms(attr[0], attr[1]);
            scope.$watch('auth', function (auth) {
                if (auth == false && auth != undefined) {
                    element.remove();
                }
            });
        }
    };
});

app.directive('memberInform', function () {
	return {
		restrict: 'EA',
		scope: {
			memberInform: '@'
		},
		controller:  function ($scope, $http, UserService) {
			$scope.getInfo = function(attr){
				$http.post('/member/info').success(function(member) {
					if(attr == 'prefix_fullName') {
						$scope.prefix_fullName = member.prefix + ' ' +member.fullName;
					}
					else if(attr == 'name2') {
						$scope.name = member.real.name ;
					}
					
				});
				};
			
			function translateNumerals(input, target) {
				var systems = {
					devanagari: 2406, tamil: 3046, kannada:  3302, 
					telugu: 3174, marathi: 2406, malayalam: 3430, 
					oriya: 2918, gurmukhi: 2662, nagari: 2534, gujarati: 2790, per: 1776
				},
				zero = 48, // char code for Arabic zero
				nine = 57, // char code for Arabic nine
				offset = (systems[target.toLowerCase()] || zero) - zero,
				output = input.toString().split(""),
				i, l = output.length, cc;

				for (i = 0; i < l; i++) {
					cc = output[i].charCodeAt(0);
					if (cc >= zero && cc <= nine) {
						output[i] = String.fromCharCode(cc + offset);
					}
				}
				return output.join("");
			}
		},
		link: function (scope, element, attrs, ctrl) {
			var attr = attrs.memberInform;
			scope.getInfo(attr);

			scope.$watch('prefix_fullName', function(prefix_fullName) {
				if(attr == 'prefix_fullName') {
					element.html('وقت بخیر، ' + prefix_fullName);
				}
				else if(attr == 'family') {
					element.html(prefix_fullName + '<i class="caret"></i>');
				}
				else if(attr == 'job') {
					element.html(prefix_fullName);
				}
			});
		}
	};
});

app.directive('personalImg', function () {
	return {
		restrict: 'EA',
		controller:  function ($scope, $http, UserService) {
			$scope.getImage = function() {
				UserService.personalImg().success(function (image){
					$scope.img = image;
				});
			};
		},
		link: function (scope, element, attrs, ctrl) {
			scope.getImage();
			scope.$watch('img', function(img) {
				if(img != undefined) {
					element.attr('src', "data:image/png;base64," + img);
				}
			});
		}
	};
});

app.directive('catablePath', function () {
	return {
		restrict: 'EA',
		controller:  function ($scope, $http) {
			$scope.getPath = function() {
				$http.get('/sessionUser').success(function(data) {
					$scope.path = "#/cartable/" + JSON.parse(data);
				});
			};
		},
		link: function (scope, element, attrs, ctrl) {
			scope.getPath();
			scope.$watch('path', function(path) {
				if(path != undefined) {
					element.attr('href', path);
				}
			});
		}
	};
});

app.directive('loading', function ($http)
{
	return {
		restrict: 'A',
		link: function (scope, elm, attrs)
		{
			scope.isLoading = function () {
				return $http.pendingRequests.length > 0;
			};

			scope.$watch(scope.isLoading, function (v)
			{
				if(v){
				    elm.fadeIn(300);
				}else{
				    elm.fadeOut(300);
				}
			});
		}
	};

});

app.directive("btnLoading", function() {
	return function(scope, element, attrs) {
		element.attr('data-loading-text', 'در حال ثبت ...');
		scope.$watch(function() {
			return scope.$eval(attrs.ngDisabled);
		}, function(newVal) {
			if (newVal) {
				return;
			} else {
				return scope.$watch(function() {
					return scope.$eval(attrs.btnLoading);
				},
				function(loading) {
					if (loading) {
						return element.button("loading");
					}
					element.button("reset");
				});
			}
		});
	};
});

app.directive('selectLoad', function () {
	return {
		restrict: 'EA',
		controller:  function ($scope, $http) {
			$scope.getRoles = function() {
				$http.get('/permissions/getRoles').success(function(role) { 
					$scope.roles = role;
				});
			};
		},
		link: function (scope, element, attrs, ctrl) {
			scope.getRoles();
			scope.$watch('roles', function(roles) {
				var list = '';
				if(roles != undefined) {
					angular.forEach(roles, function(item) {
						list += '<option value="' + item._id + '" ';
						if(scope.$eval(attrs.selectLoad) == item._id) {
							list += ' selected="true"';
						}
						list += '">' + item.name + '</option>';
					});
					element.html(list);
				}
			});
		}
	};
});

app.directive('magnifire', function ($http) {
	return {
		link: function (scope, element, attrs) {
			var random = Math.floor((Math.random() * 10000) + 1);
			var identy = 'class' + random;
			element.addClass(identy);
			var a1 = scope[attrs.magnifire];
			var images = scope.images;
			scope.$watch(function() {
				if(scope.$eval(attrs.magnifire) != undefined) {
					$('.'+identy).magnificPopup({
						items: {
						    src: '<img class="image-mag" src="data:image/png;base64,' + scope.$eval(attrs.magnifire) +'" >',
						    type: 'inline',
						    removalDelay: 300,
						    mainClass: 'mfp-fade'
						}
					});
				}
			});
		}
	};
});
app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    //scope.$emit('ngRepeatFinished');
                    //console.log(attr['funcName']);
					eval(attr['funcName']);
            });
            }
        }
    }
});

app.directive('picLoad', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind("mouseover", function(){
               // angular.element(document.getElementById('subnavPic')).attr('src',attr["picSrc"]);
                /*var img =attr["picSrc"];
               $compile(angular.element(document.getElementById(attr['picLoad'])).attr('src', "data:image/png;base64," + img));*/
                $(".itemPic").hide();
                $("#"+attr["picLoad"]).show();
				
            })
        }
    }
    
});

app.directive("addColor", function($compile){
	return function(scope, element, attrs){
		element.bind("click", function(){
			//scope.countColor++;
            scope.ware.color.push("#FFFFFF");
            
            //$compile(angular.element(document.getElementById('color-space')))(scope);
            /*angular.element(document.getElementById('color-space')).append($compile("<color-picker ng-model=ware.color["+scope.countColor+"] color-picker-format=\"'hex'\"></color-picker>")(scope));*/
		});
	};
});
app.directive("removeColor", function($compile){
	return function(scope, element, attrs){
		element.bind("click", function(){
            scope.ware.color.splice(attrs["index"], 1)
        });
	};
});
app.directive('usSpinner',   ['$http', '$rootScope' ,function ($http, $rootScope){
        return {
            link: function (scope, elm, attrs)
            {
                $rootScope.spinnerActive = false;
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (loading)
                {
                    $rootScope.spinnerActive = loading;
                    if(loading){
                        elm.removeClass('ng-hide');
                    }else{
                        elm.addClass('ng-hide');
                    }
                });
            }
        };

    }]);

app.directive('flowImageResize', function($q) {
    return {
        'require': 'flowInit',
        'link': function(scope, element, attrs) {
            scope.$flow.opts.preprocess = function (chunk) {
                chunk.fileObj.promise.then(function () {
                    chunk.preprocessFinished();// upload only then image resize is finished
                });
            };
            scope.$flow.on('filesSubmitted', function (files) {
                console.log(files);
                angular.forEach(files, function (file) {
                    var nativeFile = file.file;// instance of File, same as here: https://github.com/flowjs/ng-flow/blob/master/src/directives/img.js#L13
                    var deferred = $q.defer();
                    file.promise = deferred.promise
                    setTimeout(function () {
                        file.file = new Blob(['image content to be uploaded']);
                        deferred.resolve();
                    },5000);
                });
            })
        }
    };
});





