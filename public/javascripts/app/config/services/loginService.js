app.factory('AuthenticationService', function($http) {
	return {	
		authenticate: function(action, subject) {
			return  $http.post('/auth', {action: action, subject: subject});
		},	
		permission: function(options) {
			return  $http.post('/authAny', options);
		},
		logOut: function() {
			$http.post('/logout').
				success(function(data) {
				});

		}
	};
});

app.factory('UserService', function($http) {
    return {
	personalImg: function() {
		var personal = {section: 'personal'};
		return $http.post('/member/image', personal);
	},

	logOut: function() {
		$http.post('/logout').
			success(function(data) {
				$location.path('/login');
			});

	}
    };
});

app.factory('LookupService', function($http) {
    return {
	getList: function(val, type) {
		var listType = {name: val, type: type};
		return $http.post('/lookup/getList', listType);
	}
    };
});

app.factory('socket', function (socketFactory) {
	return socketFactory({
		//prefix: 'foo~',
		ioSocket: io.connect()
	});
});