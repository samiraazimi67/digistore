app.factory('excavationState', function($modal,$scope,$http) {
	
		goToState = function (isValid,form,nextStateIndex) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/dialogs/confirm.html',
			controller:  
			function ($scope, $log,params) {
				$scope.desc ="��� ������Ͽ�" ; 
				$scope.ok = function() {
					changeState(params.isValid,params.form,params.nextStateIndex) ;
					modalInstance.close();
				}
				$scope.cancel = function() {
					modalInstance.close();
				}
			},
			resolve: {
				params: function () {
					return {isValid:isValid,form:form,nextStateIndex:nextStateIndex};
				}
			},
			size: 'md'
		});
		
		modalInstance.result.then(function (result) {
			$scope.commentTable.reload();
		}, function () {
			$scope.commentTable.reload();
		});
	};	
	
	changeState = function (isValid,form,nextStateIndex) {
			newState = $scope.state.nextStates[nextStateIndex].id ;
			confirm = $scope.state.nextStates[nextStateIndex].confirm ;
					comment = '�ј� �� ����� ' + 
							$scope.stateExcavation.getStateById(newState).name
						+ ' ���� ����� ��� '  ;
					$http.post('/excavation/chageState' , {id:$stateParams.id,newState:newState,comment:comment,confirm:confirm})
					.success(function (data) {
						$window.history.back();
						$rootScope.runNotify('success',comment);
						$scope.loading = false;	
					});
				
		
	};
	
	createExcavation = function (isValid,form,nextStateIndex) {
		if (isValid) {  
			form.state = $scope.state.nextStates[nextStateIndex].id ;
			console.log(isValid,form);
			$http.post('/excavation/create', form).
				success(function (data) { 
					if(data.state == "success") {
						$rootScope.runNotify('success', $filter('translate')(data.message));

					} else
						$rootScope.runNotify('error', data.message);
					$scope.loading = false;
				});
		}
	};
	
	$scope.stateExcavation ={	
		states : [				
			{	id:10 		,name:"��� �������"		,visible:true	
				,nextStates:[{id:20,confirm:true,title:"���",action:createExcavation,class:"btn btn-success"}
							]			
				,role:"Excavation Suppliant"
				,desc:""
				,readOnlies:[false,false]
				,visibles:[true,true]
			} 
			,{	id:20		,name:"����� ���������"	,visible:true		
				,nextStates:[{id:1000,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:30,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:30		,name:"����� ���������"	,visible:true		
				,nextStates:[{id:20,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:40,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:40		,name:"����� ���� � �������"	,visible:true		
				,nextStates:[{id:30,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:50,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:"���ԡ ���� � ��� ������ � ����ʡ ������"			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:50		,name:"������ �����"	,visible:true		
				,nextStates:[{id:40,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:60,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:60		,name:"������"	,visible:true		
				,nextStates:[{id:50,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:70,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:70		,name:"����� ����"	,visible:true		
				,nextStates:[{id:60,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:80,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Civil Financial" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:80		,name:"����� �����"	,visible:true		
				,nextStates:[{id:70,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:90,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:90		,name:"���� ����"	,visible:true		
				,nextStates:[{id:80,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:100,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Civil Department" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:100		,name:"����� ����"	,visible:true		
				,nextStates:[{id:90,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:110,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Civil Region" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:110		,name:"����� �����"	,visible:true		
				,nextStates:[{id:100,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:120,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:120		,name:"����� �����"	,visible:true		
				,nextStates:[{id:110,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:130,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:130		,name:"����� �����"	,visible:true		
				,nextStates:[{id:120,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:140,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:140		,name:"������ �����"	,visible:true		
				,nextStates:[{id:130,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:150,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:150		,name:"����� �������"	,visible:true		
				,nextStates:[{id:140,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:160,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:160		,name:"����� ������"	,visible:true		
				,nextStates:[{id:150,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:170,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:170		,name:"����� �����"	,visible:true		
				,nextStates:[{id:160,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:180,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:180		,name:"����� �����"	,visible:true		
				,nextStates:[{id:170,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:1000,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
			,{	id:1000		,name:"����� ��� ������"	,visible:true		
				,nextStates:[{id:120,confirm:false,title:"��",action:goToState,class:"btn btn-danger"}
							,{id:140,confirm:true,title:"�����",action:goToState,class:"btn btn-success"}
							]	
				,role:"Excavation Unit" 	
				,desc:""			
				,readOnlies:[true,false]
				,visibles:[true,true]
			} 
		],
		getStateById : function(id){
			for (var i = 0 ;i < this.states.length;i++)
				if (this.states[i].id==id) 
					return this.states[i] ;
		}                                   
	}      

	
});
