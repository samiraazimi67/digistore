var app = angular.module('myApp',['ui.router', 
	'ngTable',
    'ngAnimate',
    'ngSanitize',
    'ngTagsInput',
    'color.picker',
    'ui.mask',
    'ui.bootstrap',
    'cgNotify',
    'flow',
    'ui.select',
    'pascalprecht.translate',
    'myApp.filters',
    'underscore',
    'btford.socket-io',
    'angularSpinner',
    'vMenu','checklist-model',
    'ngScrollbars','lrInfiniteScroll']).run(function($rootScope, $state, $location,$window, notify, AuthenticationService) {
    $rootScope.showBreadcrmbs=true;
    var screenWidth = $window.innerWidth;
    $rootScope.isDesktop=screenWidth>840;
    $rootScope.isAdminPage=false;
    
	document.addEventListener("keyup", function(e) {
					if (e.keyCode === 27)
						$rootScope.$broadcast("escapePressed", e.target);
				});
                
                document.addEventListener("click", function(e) {
                    $rootScope.$broadcast("documentClicked", e.target);
                });
	var history = [];
	$rootScope.runNotify = function(type, message) {
		var classes = '';
		var message = message;
		
		switch (type){
			case 'success':
				classes = 'alert-info';
				break;
			case 'info':
				classes = 'alert-info';
				break;
			case 'error':
				classes = 'alert-danger';
				break;
			case 'warning':
				classes = 'alert-warning';
				break;
		}
		
		notify({
			message: message,
			classes: classes,
			templateUrl: ''
		});
    };
	
 
	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, from, fromParams){
		
		/*if(toState.data.show != undefined)
		  $rootScope.showBreadcrmbs=toState.data.show;
        else*/
            $rootScope.showBreadcrmbs=toState.data.show;
        /*if(!$rootScope.isDesktop){
            $rootScope.vmenuVisible = false;
            $rootScope.vprofileVisible=false;
        }*/
        console.log(toState.data.before);
       console.log($state.href($state.get(toState.data.before).name));
        $("#returnUrl").attr("href","#"+$state.href($state.get(toState.data.before).name));
        
        var base = "<li><a href='#'>داشبورد<i class='glyphicon glyphicon-dashboard' style='margin-right: 5px;'></a></i>";
            $("#dashboard").html(toState.data.name);
            var li = genDashboard(toState.name);
            
            li += base;
            $("#breadcrumb").html(li);
            
		function genDashboard (stateName) {
            
			var dashboard = '';
			var state = $state.get(stateName);
            //console.log(state);
			var before = state.data.before;
			if (before == '') {
				return dashboard;
			}
			dashboard += "<li><a href='#" + $state.href(state.name) + "'>" + state.data.breadcrumb + "<i class='glyphicon glyphicon-" + state.data.icon + "'></i></a></li>";
			return dashboard + genDashboard(before);
		};
		AuthenticationService.authenticate(toState.data.action, toState.data.subject).success(function (data) {

                        if (toState.data.authenticate && !data.auth) {
                                if (navigator.appVersion.indexOf('MSIE') !== -1) {
                                        $state.transitionTo('login');
                                        AuthenticationService.logOut();
                                        $rootScope.runNotify('warning', 'لطفا از مرورگر استاندارد مانند Chrome يا FireFox استفاده نماييد.');
                                }
                                else if (data.login) {
                                        $state.transitionTo('login');
                                        $rootScope.runNotify('warning', 'شما دسترسي به اين بخش را نداريد!');
                                }
                                else {
                                        $state.transitionTo('login');
                                        $rootScope.runNotify('warning', 'شما بايستي وارد سيستم شويد.');
                                }
                                event.preventDefault();

                        }
                        $rootScope.login=data.login;
                });
	});
	
});;

app.config(
	function($stateProvider,$controllerProvider ,$urlRouterProvider, flowFactoryProvider, $translateProvider) {
		
		
		flowFactoryProvider.defaults = {
			target: '/upload',
			chunkSize: 1024*1024*1024,
			permanentErrors: [404, 500, 501],
			maxChunkRetries: 1,
			chunkRetryInterval: 5000,
			simultaneousUploads: 4,
			testChunks:false
		};
		flowFactoryProvider.on('catchAll', function (event) {
			console.log('catchAll', arguments);
		});
		$urlRouterProvider.otherwise('home/mainHome');
		
		
		$stateProvider.state('home',{
			url: '/home',
			templateUrl: 'partials/home.html',
			controller: 'wareController',
            data: {
                    authenticate: false,
                    action: '',
                    subject: '',
                    name: 'مشتری',
                    breadcrumb: 'مشتری',
                    show:false,
                    icon: '',
                    before: ''
                },
        })
        .state('home.mainHome',{
            url:'/mainHome',
            templateUrl:'partials/mainHome.html',
            //controller:'wareController',
            data:{
                authenticate: false,
                action: '',
                subject: '',
                name: 'مشتری',
                breadcrumb: 'مشتری',
                show:false,
                icon: '',
                before: ''
            }
        })
        .state('home.shopList',{
            url:'/shopList',
            templateUrl:'partials/shop/shopList.html',
            //controller:'shopController',
            data:{
                authenticate: false,
                action: '',
                subject: '',
                name: 'لیست فروشگاه ها',
                breadcrumb: 'لیست فروشگاه ها',
                show:true,
                icon: '',
                before: ''
            }
        })
        .state('home.wareList', {
            url: '/customer/wareList',
                templateUrl: 'partials/ware/customerWareList.html',
                //controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: '',
                        breadcrumb: 'لیست کالاهای موجود',
                        icon: '',
                        show:false,
                        before: ''
                    },
        })
        /*.state('home.shopwareList', {
            url: '/customer/shopwareList/?supplier',
                templateUrl: 'partials/ware/customerWareList.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: '',
                        breadcrumb: 'لیست کالاهای موجود',
                        icon: '',
                        show:false,
                        before: 'home.shopList'
                    },
        })
        .state('home.searchwareList', {
            url: '/customer/searchwareList/?tag',
                templateUrl: 'partials/ware/customerWareList.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: '',
                        breadcrumb: 'لیست کالاهای موجود',
                        icon: '',
                        show:false,
                        before: ''
                    },
        })*/
        .state('home.wareDetail', {
            url: '/customer/wareDetail/:id',
                templateUrl: 'partials/ware/wareDetail.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'جزپیات کالا',
                        breadcrumb: 'جزییات کالا',
                        icon: '',
                        show: true,
                        before: 'home.wareList'
                    },
        })
        
        
        $stateProvider.state('login',{
			url: '/login',
			templateUrl: 'partials/admin/login.html',
			controller: 'shopController',
            data: {
                    authenticate: false,
                    action: '',
                    subject: '',
                    name: 'مشتری',
                    breadcrumb: 'مشتری',
                    icon: '',
                    show:false,
                    before: ''
                },
        });	
        
        /*$stateProvider.state('customerHome', {
                url: '/customerHome/:wareTypeId?catId',
                templateUrl: 'partials/customer/index.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'مشتری',
                        breadcrumb: 'مشتری',
                        icon: '',
                        before: ''
                    },
            })
        .state('customerHome.wareList', {
            url: '/customer/wareList/:wtId',
                templateUrl: 'partials/ware/customerWareList.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'مشتری',
                        breadcrumb: 'مشتری',
                        icon: '',
                        before: ''
                    },
        })
        .state('customerHome.wareDetail', {
            url: '/customer/wareDetail/:id',
                templateUrl: 'partials/ware/wareDetail.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'مشتری',
                        breadcrumb: 'مشتری',
                        icon: '',
                        before: ''
                    },
        });
        */
        $stateProvider.state('adminHome', {
                url: '/admin',
                templateUrl: 'partials/admin/index.html',
                controller:'shopController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'کارتابل فروشگاه',
                        breadcrumb: 'کارتابل فروشگاه',
                        icon: '',
                        show:true,
                        before: ''
                    },
            })
            .state('adminHome.shops', {
                url: '/adminShops',
                templateUrl: 'partials/admin/shopList.html',
                controller:'shopController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'لیست فروشگاه ها',
                        breadcrumb: 'لیست فروشگاه ها',
                        show:true,
                        icon: '',
                        before: 'adminHome'
                    },
            })
        .state('adminHome.createShop', {
                url: '/createShop',
                templateUrl: 'partials/shop/form.html',
                controller:'shopController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ایجاد فروشگاه',
                        breadcrumb: 'ایجاد فروشگاه',
                        icon: '',
                        show:true,
                        before: 'adminHome.shops'
                    },
                
            })
        .state('adminHome.editShop', {
                url: '/shop/edit/:id',
                templateUrl: 'partials/shop/form.html',
                controller:'shopController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ویرایش فروشگاه',
                        breadcrumb: 'ویرایش فروشگاه',
                        icon: '',
                        show:true,
                        before: 'adminHome.shops'
                    },
            })
        .state('adminHome.shopRole', {
                url: '/shop/role/:id',
                templateUrl: 'partials/shop/roles.html',
                controller:'shopController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'سطح دسترسی فروشگاه ها',
                        breadcrumb: 'مشتری',
                        icon: '',
                        show:true,
                        before: 'adminHome.shops'
                    },
            })
        .state('adminHome.wareTypes', {
                url: '/wareType/list',
                templateUrl: 'partials/ware/wareTypeList.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'لیست انواع اجناس',
                        breadcrumb: 'لیست انواع اجناس',
                        icon: '',
                        show:true,
                        before: 'adminHome'
                    },
            })
        .state('adminHome.createWareType', {
                url: '/wareType/create',
                templateUrl: 'partials/ware/wareTypeform.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ایجاد نوع جنس',
                        breadcrumb: 'ایجاد نوع جنس',
                        icon: '',
                        show:true,
                        before: 'adminHome.wareTypes'
                    },
            })
        .state('adminHome.editWareType', {
                url: '/wareType/edit/:id',
                templateUrl: 'partials/ware/wareTypeform.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ویرایش نوع جنس',
                        breadcrumb: 'ویرایش نوع جنس',
                        icon: '',
                        show:true,
                        before: 'adminHome.wareTypes'
                    },
            })
        
        .state('adminHome.wareList', {
                url: '/ware/list/?supplier',
                templateUrl: 'partials/ware/list.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'لیست اجناس فروشگاه',
                        breadcrumb: 'لیست اجناس فروشگاه',
                        icon: '',
                        show:true,
                        before: 'adminHome'
                    },
            })
        
        .state('adminHome.createWare', {
                url: '/ware/create/?supplier',
                templateUrl: 'partials/ware/form.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ایجاد کالا',
                        breadcrumb: 'ایجاد کالا',
                        icon: '',
                        show:true,
                        before: 'adminHome.wareList'
                    },
            })
        .state('adminHome.editWare', {
                url: '/ware/edit/:id',
                templateUrl: 'partials/ware/form.html',
                controller:'wareController',
                data: {
                        authenticate: false,
                        action: '',
                        subject: '',
                        name: 'ویرایش کالا',
                        breadcrumb: 'ویرایش کالا',
                        icon: '',
                        show:true,
                        before: 'adminHome.wareList'
                    },
            })
        .state('adminHome.roleManager',{
            url: '/roleManager',
            templateUrl: 'partials/admin/roleManager.html',
            controller:'roleManagerController',
            data: {
                authenticate: false,
                action: '',
                subject: '',
                name: 'Role Manager',
                breadcrumb: 'Role Manager',
                icon: '',
                show:true,
                before: 'adminHome'
            },
        })
	}
);
moment.loadPersian();
 app.filter('jalaliDate', function () {
            return function (inputDate, format) {
                var t=String(inputDate)
                var date = moment(t, 'jYYYYjMMjDD.HHmmss');
                return date.fromNow() + " " + date.format(format);
            }
        });
app.filter('jalaliFromNow', function () {
            return function (inputDate, format) {
                var t=String(inputDate)
                var date = moment(t, 'jYYYYjMMjDD.HHmmss');
                return date.fromNow();
            }
        });
app.controller('main', function( $scope,$http,$rootScope,$modal, $filter,$location,$state,$window) {
	$scope.home = 'partials/index.html';
    $rootScope.isTouchDevice = 'ontouchstart' in document.documentElement;
    $rootScope.wareTypeList = {};
    $rootScope.suppliers={};
    $scope.logUrl='logoutPage.html';
   $rootScope.showBreadcrmbs=true;
    $rootScope.targetjs = function (ware) {
        
       $('#image'+ware._id).addClass("targetPic");
        console.log($('#image'+ware._id));
  };
    $http.post('/wareType/list').
				success(function(result){
					$rootScope.wareTypeList = result.data ;
				});
    $http.post('/shop/listTable',{'orderBy':{'off':-1}}).
				success(function(result){
					$rootScope.suppliers = result.data ;
				});
    $rootScope.closeTarget= function() {
        $(".targetPic").removeClass("targetPic");
    }
    $http.get('/lookup').success(function (lookup) {
        $scope.lookup = lookup;
        $scope.catList=$scope.lookup.categories;
        
    });
    $http.get('/menu').success(function (menu) {
        $scope.menu = menu;
    });
    
	$scope.SPAUrl = function() {
		
			return $scope.home;
		};
    
    $scope.init = function () {
                $http.get('/shop/session').success(function (res) {
                        $rootScope.me = res.user;
                        $rootScope.auth=res.auth;
                        $rootScope.jDate = res.jDate;
                        if (res.me == 'undefined') {
                                $state.transitionTo('login');
                        }

                });
        };
     $scope.logout = function logout() {
                $http.post('/logout').
                        success(function (data) {
                                $rootScope.me = {};
                                $rootScope.auth=false;
                                $rootScope.vprofileVisible = false;
                                $location.path('/login');
                        });
        };
   
       $scope.config = {
            //autoHideScrollbar: true,
            theme: 'minimal-dark',
            advanced:{
                updateOnContentResize: true
            },
                //setHeight: 400,
                setWidth: 220,
                scrollInertia: 0
        };
       $scope.leftSideScrollConfig = {
            autoHideScrollbar: true,
            theme: 'dark',
            advanced:{
                updateOnContentResize: true
            },
                //setHeight: 400,
                setWidth: '100%',
                scrollInertia: 0
        };
       
        
        
});

app.controller('vMenuController', function( $scope,$http,$rootScope, $filter,$location,$state,$window) {
    
    $scope.logoutPage='logoutPage.html';
	$rootScope.searchMenuVisible = false;
    $rootScope.vprofileVisible=false;
    $rootScope.vmenuVisible = $rootScope.isDesktop;
    
    $rootScope.close = function() {
        $rootScope.searchMenuVisible = false;
        $rootScope.vprofileVisible = false;
    };
    $rootScope.showvheadMenu= function(headId,e){
        if($rootScope.headIdVisible == headId)
            $rootScope.headIdVisible=-1
        else
            $rootScope.headIdVisible=headId;
        
    };
    $rootScope.showSearchMenu = function(e) {
        if($rootScope.searchMenuVisible == true)
        {
            $rootScope.searchMenuVisible = false;
            e.stopPropagation();
        }
        else
        {
            $rootScope.searchMenuVisible=true;
            $rootScope.vprofileVisible = false;
            e.stopPropagation();
        }
    };
    $rootScope.showProfileMenu= function(e) {
        if($rootScope.vprofileVisible == true)
        {
            $rootScope.vprofileVisible = false;
            e.stopPropagation();
        }
        else
        {
            $rootScope.vprofileVisible=true;
            $rootScope.searchMenuVisible = false;
            e.stopPropagation();
        }
    };
    $rootScope.showvMenu = function(e) {
        if($rootScope.vmenuVisible == true && !$rootScope.isDesktop)
        {
            $rootScope.vmenuVisible = false;
            e.stopPropagation();
        }
        else
        {
            $rootScope.vmenuVisible=true;
            e.stopPropagation();
        }
    };
    $(document).on('click', '.profile-menu a', function (e) {
          $rootScope.vprofileVisible = false;
            e.stopPropagation();
    });
    
   $(document).on('click', '.content', function (e) {
       if ($(e.target).closest('menu').length)
            return;
        _close()
    });
    $rootScope.$on("escapePressed", _close);
    function _close() {
        $scope.$apply(function() {
            $rootScope.close(); 
        });
    }
});
