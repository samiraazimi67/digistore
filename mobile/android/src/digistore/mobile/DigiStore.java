package digistore.mobile;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class DigiStore extends Activity
{
    private WebView webview ;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        webview = (WebView) findViewById(R.id.webView1);

        webview.setInitialScale(100);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);
        
        webview.loadUrl("http://192.168.1.100:3000");

    }
}
